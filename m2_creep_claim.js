// m2_creep_claim

var m2_creep_claim = {
	sName : 'module_creep_claim',
	rMission : [
		{
			sHome: "E22N16",
			sSpawn: "s03b",
			sTarget: "E21N16"
		},
		{
			sHome: "E22N21",
			sSpawn: "s10b",
			sTarget: "E23N21"
		}


	],
	rBody: [CLAIM,CLAIM,CLAIM,CLAIM,CLAIM,CLAIM, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE],
	sCurrentWorker: [],
	sRole: 'claimering',
	creephook : function (creep, postauth) {
		if (creep.memory.role == m2_creep_claim.sRole)
		{
			m2_creep_claim.sCurrentWorker.push(creep.memory.sTarget);
			if (creep.room.name != creep.memory.sTarget)
				creep.moveTo(Game.flags[creep.memory.sTarget]);
			else
			{
				switch (creep.reserveController(creep.room.controller)) {
		    		case ERR_NOT_IN_RANGE:
		    			creep.moveTo(creep.room.controller);
		    			break;
		    		case ERR_INVALID_TARGET:
		    			if (creep.attackController(creep.room.controller) == ERR_NOT_IN_RANGE) {
		    				creep.moveTo(creep.room.controller);
		    			}
		    			break;
		    		default:
		    			break;
		    	}
			}
		}
	},
	run : function (m2) {
		for (var iUnit in m2_creep_claim.rMission)
		{
			if (m2_creep_claim.sCurrentWorker.indexOf(m2_creep_claim.rMission[iUnit].sTarget) < 0)
			{
				if (!Game.spawns[m2_creep_claim.rMission[iUnit].sSpawn].spawning) {
					let sBuildReturn = Game.spawns[m2_creep_claim.rMission[iUnit].sSpawn].spawnCreep(
						m2_creep_claim.rBody,
							'Emanuel ' + iUnit +
							Math.random().toString(36).substr(2, 3),
						{
							memory : {
								role:m2_creep_claim.sRole,
								hook: m2_creep_claim.sName,
								sTarget:m2_creep_claim.rMission[iUnit].sTarget,
								sSpawn:m2_creep_claim.rMission[iUnit].sSpawn,
								sHome:m2_creep_claim.rMission[iUnit].sHome
							}
						}
					);
					console.log(m2_creep_claim.sRole + '(m2) Built in ['+m2_creep_claim.rMission[iUnit].sHome+ '] for ['+m2_creep_claim.rMission[iUnit].sTarget+'] : '+sBuildReturn);
				}
			}
		}
	}
};
module.exports = m2_creep_claim;
