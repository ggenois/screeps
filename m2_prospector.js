// m2_prospector.js

var m2_prospector = {
	isPresent : function (sRoom) {

		for (let iBaseRoom in Memory.config.oBases) {
			if (Memory.config.oBases[iBaseRoom].sRoom == sRoom) {
				return true;
			}
		}

		for (let iRoom in Memory.config.oMap) {
			if (Memory.config.oMap[iRoom].sRoom == sRoom) {
				return true;
			}
		}
		return false;
	},

	meminits : function (m2, iBase) {
		if (
			Memory.config.oBases[iBase].oPosition == undefined ||
			Memory.config.oBases[iBase].oPosition == null
		) {
			Memory.config.oBases[iBase].oPosition = {
				iX: Memory.config.oBases[iBase].sRoom.substr(1, 2),
				iY: Memory.config.oBases[iBase].sRoom.substr(4, 2)
			};
		}
		if (
			Memory.config.oBases[iBase].rProspectorStack == undefined ||
			Memory.config.oBases[iBase].rProspectorStack == null
		) {
			Memory.config.oBases[iBase].rProspectorStack = Array();
		}
		if (
			Memory.config.oBases[iBase].iProspectorCounter == undefined ||
			Memory.config.oBases[iBase].iProspectorCounter == null
		) {
			Memory.config.oBases[iBase].iProspectorCounter = 0;
		}
	},

	basehook : function (m2, iBase) {
		if (
			Memory.config.oMap == undefined ||
			Memory.config.oMap == null ||
			!Memory.config.bRunProspector ||
			Memory.config.oBases[iBase].iProspectorStatus == 2
		) {
			return (false);
		}
		// mem inits
		m2_prospector.meminits(m2, iBase);
		// For each base, use the obseverver to look outside if it's possible
		if (Memory.config.oBases[iBase].sOberverId != null) {
			// set current item
			if (Memory.config.oBases[iBase].rProspectorStack.length > 0) {
				var sRoomName = Memory.config.oBases[iBase].rProspectorStack[0];
				// Remove the current item from stack.
				Memory.config.oBases[iBase].rProspectorStack =
					m2.tools.spliceh(
						Memory.config.oBases[iBase].rProspectorStack
				);
				if (Memory.config.oMap.indexOf(sRoomName) >= 0) {
					return (false);
				}
			} else {
				if (Memory.config.oBases[iBase].iProspectorCounter == 0) {
					var sRoomName = Memory.config.oBases[iBase].sRoom;
					console.log("m2_prospector lauched for " + sRoomName);
					Memory.config.oBases[iBase].iProspectorStatus = 1;
				} else {
					Memory.config.oBases[iBase].iProspectorCounter = 0;
					Memory.config.oBases[iBase].iProspectorStatus = 2;
					return (false);
				}
			}
			// set Range Limits
			let oRL = {
				iXmin: Memory.config.oBases[iBase].oPosition.iX - Memory.config.iProspectorMaxRadius,
				iXmax: Memory.config.oBases[iBase].oPosition.iX + Memory.config.iProspectorMaxRadius,
				iYmin: Memory.config.oBases[iBase].oPosition.iY - Memory.config.iProspectorMaxRadius,
				iYmax: Memory.config.oBases[iBase].oPosition.iY + Memory.config.iProspectorMaxRadius
			};
			// create map object
			let oMapObject = {
				sRoom: sRoomName,
				sOwner: null,
				bOwned : false,
				iX : sRoomName.substr(1, 2),
				iY : sRoomName.substr(4, 2),
				rEnergySources: Array(),
				rMineralSource: Array(),
				iMineralType : null,
				rGates : Array(),
				sSrc: Memory.config.oBases[iBase].sRoom
			};
			if (Game.rooms[sRoomName].controller != undefined) {
				if (Game.rooms[sRoomName].controller.owner != undefined) {
					if (Game.rooms[sRoomName].controller.owner.username != undefined) {
						oMapObject.sOwner = Game.rooms[sRoomName].controller.owner.username;
						oMapObject.bOwned = true;
					}
				}
				if (oMapObject.sOwner == null && Game.rooms[sRoomName].controller.reservation != undefined) {
					if (Game.rooms[sRoomName].controller.reservation.username != undefined) {
						oMapObject.sOwner = Game.rooms[sRoomName].controller.reservation.username;
					}
				}
			}
			// Add source to the base
			let oSources = Game.rooms[sRoomName].find(FIND_SOURCES);
			if (oSources) {
				for (let iS in oSources) {
					oMapObject.rEnergySources.push(oSources[iS].id);
				}
			}
			// Add Mineral to the base
			let oMinerals = Game.rooms[sRoomName].find(FIND_MINERALS);
			if (oMinerals) {
				for (let iMI in oMinerals) {
					oMapObject.rMineralSource.push(oMinerals[iMI].id);
					oMapObject.iMineralType = oMinerals[iMI].mineralType;
				}
			}
			// Add rGates to maps object
			let exits = Game.map.describeExits(sRoomName);
			if (exits) {
				for (let ex in exits) {
					let oGate = {
						sRoom: exits[ex],
						iD: ex,
						iX: exits[ex].substr(1, 2),
						iY: exits[ex].substr(4, 2)
					};
					oMapObject.rGates.push(oGate);
					if (
						oGate.iX > oRL.iXmin && oGate.iX < oRL.iXmax &&
						oGate.iY > oRL.iYmin && oGate.iY < oRL.iYmax
					) {
						if (!m2_prospector.isPresent(exits[ex])) {
							Memory.config.oBases[iBase].rProspectorStack.push(
								exits[ex]
							);

						}
					}
				}
			}
			// Prepare for the next scan
			if (Memory.config.oBases[iBase].rProspectorStack.length > 0) {
				let oObserver = Game.getObjectById(
					Memory.config.oBases[iBase].sOberverId
				);
				if (oObserver) {
					oObserver.observeRoom(
						Memory.config.oBases[iBase].rProspectorStack[0]
					);
				}
			}
			Memory.config.oBases[iBase].iProspectorCounter =
				Memory.config.oBases[iBase].iProspectorCounter + 1;
			Memory.config.oMap.push(oMapObject);
		} else {
			// no obvserver.
			Memory.config.oBases[iBase].iProspectorStatus = 2;
		}
	},

	selector_sub : function (m2, oMap) {
		if (oMap.sOwner != null) {
			if (oMap.sOwner == m2.config.sUsername && !oMap.bOwned) {
				return (true);
			} else {
				return (false);
			}
		} else {
			return (true);
		}

	},

	// Select potential bases in the main map object
	selector : function(m2) {
		Memory.config.oPotentialBases = Array();
		var	rPotentialBase = Array();
		for (let iRoom in Memory.config.oMap) {
			if (
				m2_prospector.selector_sub(m2, Memory.config.oMap[iRoom]) &&
				Memory.config.oMap[iRoom].rEnergySources.length == 2

			) {
				console.log("Prospector > Selector; Analysing room " + Memory.config.oMap[iRoom].sRoom);
				let oPath = Game.map.findRoute(
					Memory.config.oMap[iRoom].sRoom,
					Memory.config.oMap[iRoom].sSrc
				);
				let rPath = Array();
				if (oPath) {
					for (let iP in oPath) {
						rPath.push(oPath[iP].room);
					}
				} else {
					console.log("Prospector > selector; No path found.");
				}
				let oProspect = {

					sRoom : Memory.config.oMap[iRoom].sRoom,
					iMapIndex : iRoom,
					oBuildInfo : null,
					rPath : rPath
				};

				// Check for the closest way
				for (let iProspect in rPotentialBase) {
					if (
						rPotentialBase[iProspect].sRoom ==
						Memory.config.oMap[iRoom].sRoom
					) {
						if (
							rPath.length < rPotentialBase[
								iProspect
							].rPath.length
						) {
							// disable entry
							rPotentialBase[
								iProspect
							].sRoom = null;

						}
					}
				}
				rPotentialBase.push(oProspect);
				console.log("Potential base pushed into temps value");
			}
		}

		for (let iB in rPotentialBase) {
			if (rPotentialBase[iB].sRoom != null) {
				console.log("Potential bases added to memory");
				Memory.config.oPotentialBases.push(rPotentialBase[iB]);
			}
		}
		Memory.config.oProspectorEngine = null;
		Memory.config.oRouhBase = Array();
		Memory.config.iProspectorStep = 2;
		console.log("m2_prospector: Finished to select.");
	},

	checkForSpot : function (sRoom, iBx, iBy, oTerrain) {
		console.log("checkForSpot...");
		let iRoomW = 50;
		let iRoomH = 50;

		let iCenterX = iRoomW / 2;
		let iCenterY = iRoomH / 2;

		let iBaseW = 11;
		let iBaseH = 13;


		let iFromX = iCenterX - (iBaseW - 1) / 2;
		let iFromY = iCenterY - (iBaseH - 1) / 2;

		iFromX = iFromX + iBx;
		iFromY = iFromY + iBy;

		let x = iFromX;
		let y = iFromY;


		while (x < iFromX + iBaseW) {
			if (x >= iRoomW) {
				return false;
			}
			while (y < iFromY + iBaseH) {
				if (y >= iRoomH) {
					return false;
				}
				switch(oTerrain.get(x,y)) {
					case TERRAIN_MASK_WALL:
						new RoomVisual(sRoom).circle(x,y, {fill: 'transparent', radius: 0.5, fill: '#ff0000'});
						return false;
						break;
					case TERRAIN_MASK_SWAMP:
						new RoomVisual(sRoom).circle(x,y, {fill: 'transparent', radius: 0.5, fill: '#ffc900'});
						break;
					case 0:
						new RoomVisual(sRoom).circle(x,y, {fill: 'transparent', radius: 0.5, fill: '#5b9a50'});
						break;
				}

				y = y + 1;
			}
			y = iFromY;
			x = x + 1;
		}
		return true;
	},

	/*
		Only search one room, one possible place, by tick.

		x
		1 ^ 1 = 1

		xx
		xx
		2 ^ 2 = 4

		xxx
		xxx
		xxx
		3 ^ 3 = 9
	*/
	searchSlow : function (m2, sRoom, oTerrain) {
		console.log("searchSlow...");
		if (Memory.config.oProspectorEngine == undefined || Memory.config.oProspectorEngine == null) {
			Memory.config.oProspectorEngine = {
				iStatus: 0,
				iStep : 0,
				iRadius: 1,
				testedStack : Array()
			};
		}

		// get x y to check from step position
		var iX = Memory.config.oProspectorEngine.iStep % Memory.config.oProspectorEngine.iRadius;
		var iY = Memory.config.oProspectorEngine.iStep / Memory.config.oProspectorEngine.iRadius;
		iY = iY.toFixed(0) - 1;

		console.log("1: x"+ iX + " y" + iY);

		// Keep in the middle
		var iGetBack = 0;
		if (Memory.config.oProspectorEngine.iRadius > 1) {
			iGetBack = Memory.config.oProspectorEngine.iRadius / 2;
			iGetBack = iGetBack.toFixed(0);
		}

		// Ajust to a base 0 and align.
		iX = iX - 1 - iGetBack;
		iY = iY - 1 - iGetBack;

		console.log("2: x"+ iX + " y" + iY + " ("+iGetBack+")");

		var iAlreadyDone = false;
		for (let iStack in Memory.config.oProspectorEngine.testedStack) {
			if (
				Memory.config.oProspectorEngine.testedStack[iStack].x == iX &&
				Memory.config.oProspectorEngine.testedStack[iStack].y == iY
			) {
				iAlreadyDone = true;
				break ;
			}
		}
		var bFound = false;
		if (!iAlreadyDone) {
			console.log("Testing "+ iX + "x "+iY+"y..");
			// test the new given location
			bFound = m2_prospector.checkForSpot(
				sRoom,
				iX,
				iY,
				oTerrain
			);
			console.log(bFound);
			Memory.config.oProspectorEngine.testedStack.push({
				x: iX,
				y: iY
			});
			new RoomVisual(sRoom).rect(25 + iX - 5, 25 + iY - 6, 10, 12);
		} else {
			console.log("Object already tested. Skipping");
			
		}

		if (bFound) {
			console.log("item found. Calling foundBuildable");
			m2_prospector.foundBuildable(m2, sRoom, iX, iY);
			// Found it ! Stop right here !

		} else {
			console.log("spot not found.");
			if (Memory.config.oProspectorEngine.iStep <= Memory.config.oProspectorEngine.iRadius * Memory.config.oProspectorEngine.iRadius) {
				console.log("Still in the radius. step into object + 1...");
				// Still in the radius
				Memory.config.oProspectorEngine.iStep = Memory.config.oProspectorEngine.iStep + 1;
			} else {
				// Not in the grid anymore.
				console.log("Got outside the grid (radius). Augment radius...");
				Memory.config.oProspectorEngine.iStep = 0;
				Memory.config.oProspectorEngine.iRadius = Memory.config.oProspectorEngine.iRadius + 1;
			}
		}

		if (Memory.config.oProspectorEngine.iRadius > 25) {
			console.log("Outside maximum radius for test...");
			m2_prospector.notFoundBuildable(m2, sRoom);
		}
		if (iAlreadyDone) {
			console.log("Allready done. Striggering again... (disabled)");
			//m2_prospector.searchSlow(m2, sRoom, oTerrain);
		}

	},

	notFoundBuildable : function (m2, sRoom) {
		console.log("didn't Found a spot in room " + sRoom);
		Memory.config.oProspectorEngine = null;
		Memory.config.oPotentialBases = m2.tools.spliceh(Memory.config.oPotentialBases);
	},

	foundBuildable : function (m2, sRoom, x, y) {

		var oRoom = Memory.config.oPotentialBases[0];

		var oPBase = {
			sRoom: sRoom,
			iMapIndex: oRoom.iMapIndex,
			rPath: Array(),
			iX: x,
			iY: y
		};

		for (let iPath in oRoom.rPath) {
			oPBase.rPath.push(oRoom.rPath[iPath]);
		}
		console.log("Found a spot in room " + sRoom);
		Memory.config.oRouhBase.push(oPBase);
		Memory.config.oProspectorEngine = null;
		Memory.config.oPotentialBases = m2.tools.spliceh(Memory.config.oPotentialBases);
		new RoomVisual(sRoom).rect(25 + x - 5, 25 + y - 6, 10, 12);

	},


	// Find a suitable spot to build
	isRoomBuildable : function (m2) {

		if (Memory.config.oPotentialBases.length > 0) {
			var iRbIndex = -1;
			var oRoom = Memory.config.oPotentialBases[0];
			for (let iRb in Memory.config.oRouhBase) {
				if (Memory.config.oRouhBase[iRb].sRoom == oRoom.sRoom) {
					iRbIndex = iRb;
				}
			}
			if (iRbIndex >= 0) {
				console.log("isRoomBuildable: Base allready added to oRouhBase");
				Memory.config.oPotentialBases = m2.tools.spliceh(Memory.config.oPotentialBases);
			} else {
				console.log("isRoomBuildable: Searching into "+oRoom.sRoom+"...");
				let oTerrain = Game.map.getRoomTerrain(oRoom.sRoom);
				m2_prospector.searchSlow(m2, oRoom.sRoom, oTerrain);
			}
		} else {
			console.log("No more oPotentialBases. Going to step 3.");
			Memory.config.iProspectorStep = 3;
		}
	},

	displayFuturBases : function (m2) {
		for (let iRouh in Memory.config.oRouhBase) {
			if (
				Memory.config.oRouhBase[iRouh].iX != null &&
				Memory.config.oRouhBase[iRouh].iY != null &&
				Memory.config.oRouhBase[iRouh].sRoom != null

			)
			new RoomVisual(Memory.config.oRouhBase[iRouh].sRoom).rect(
				25 + Memory.config.oRouhBase[iRouh].iX - 5,
				25 + Memory.config.oRouhBase[iRouh].iY - 6,
				10,
				12
			);
		}
	},

	run : function (m2) {
		if (Memory.config.oMap == undefined || Memory.config.oMap == null) {
			Memory.config.oMap = Array();
		}
		if (Memory.config.oPotentialBases == undefined || Memory.config.oPotentialBases == null) {
			Memory.config.oPotentialBases = Array();
		}

		if (Memory.config.oRouhBase == undefined || Memory.config.oRouhBase == null) {
			Memory.config.oRouhBase = Array();
		}

		if (Memory.config.iProspectorMaxRadius == undefined || Memory.config.iProspectorMaxRadius == null) {
			Memory.config.iProspectorMaxRadius = 2;
		}

		if (Memory.config.bRunProspector == undefined || Memory.config.bRunProspector == null) {
			Memory.config.bRunProspector = false;
			Memory.config.oMap = Array();
			Memory.config.oPotentialBases = Array();
			Memory.config.oProspectorEngine = null;
			Memory.config.iProspectorStep = 0;

			for (let iBase in Memory.config.oBases) {
				Memory.config.oBases[iBase].iProspectorStatus = 0;
				Memory.config.oBases[iBase].rProspectorStack = Array();
				Memory.config.oBases[iBase].iProspectorCounter = 0;
			}
		}

		if (Memory.config.bRunProspector == true) {
			let ibS = false;
			for (let iBase in Memory.config.oBases) {
				if (
					Memory.config.oBases[iBase].iProspectorStatus == undefined ||
					Memory.config.oBases[iBase].iProspectorStatus == null
				) {
					Memory.config.oBases[iBase].iProspectorStatus = 1;
					ibS = true;
				} else if (Memory.config.oBases[iBase].iProspectorStatus == 1 || Memory.config.oBases[iBase].iProspectorStatus == 0) {
					ibS = true;
				}
			}
			if (ibS == false) {
				switch (Memory.config.iProspectorStep) {
					case 0:
						console.log("m2_prospector: (0) Finished to map.");
						Memory.config.iProspectorStep = 1;
						break ;
					case 1:
						console.log("m2_prospector: (1) Callling selector...");
						m2_prospector.selector(m2);
						break ;
					case 2:
						console.log("m2_prospector: (2) Callling selector...");
						if (Memory.config.oPotentialBases.length == 0) {
							console.log("cleaning up...");
							Memory.config.iProspectorMaxRadius = Memory.config.iProspectorMaxRadius + 1;
							Memory.config.oMap = Array();
							for (let iBaseb in Memory.config.oBases) {
								Memory.config.oBases[iBaseb].rProspectorStack = Array();
								Memory.config.oBases[iBaseb].iProspectorCounter = 0;
								Memory.config.oBases[iBaseb].iProspectorStatus = 1;
							}
							Memory.config.iProspectorStep = 0;
							console.log("m2_prospector: nothing found. Starting agin with wider view. " + Memory.config.iProspectorMaxRadius);
						} else {
							console.log("There is potentatial base.");
							console.log(Memory.config.oPotentialBases[0].sRoom)
						}
						m2_prospector.isRoomBuildable(m2);
					default:
						Memory.config.bRunProspector = null;
						break ;
				}
			}
		}
		m2_prospector.displayFuturBases(m2);
	}
};

module.exports = m2_prospector;
