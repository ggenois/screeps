// m2_tower

var m2_tower = {
	sName : 'm2_tower',
	myUserName : "c01",
	init : function (m2, iBase) {
		if (Memory.config.oBases[iBase].activeDefence == undefined || Memory.config.oBases[iBase].activeDefence == null) {
			Memory.config.oBases[iBase].activeDefence = false;
		}
		if (
			Memory.config.oBases[iBase].bSouspicious == undefined ||
			Memory.config.oBases[iBase].bSouspicious == null
		) {
			Memory.config.oBases[iBase].bSouspicious = false;
		}
		if (Memory.config.oBases[iBase].rHostiles == undefined || Memory.config.oBases[iBase].rHostiles == null) {
			Memory.config.oBases[iBase].rHostiles = Array();
		}
		if (Memory.config.oBases[iBase].rRepairs == undefined || Memory.config.oBases[iBase].rRepairs == null) {
			Memory.config.oBases[iBase].rRepairs = Array();
		}
		if (Memory.config.oBases[iBase].rTowers == undefined || Memory.config.oBases[iBase].rTowers == null)
		{
			let rTowers = Game.rooms[Memory.config.oBases[iBase].sRoom].find(
				FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}}
			);
			Memory.config.oBases[iBase].rTowers = Array();
			if (rTowers.length > 0) {
				for (let iTowers in rTowers) {
					Memory.config.oBases[iBase].rTowers.push(rTowers[iTowers].id);
				}
			}
		}
	},

	fight : function (m2, iBase) {
		if (Memory.config.oBases[iBase].rHostiles.length > 0) {
			let oTarget = Game.getObjectById(Memory.config.oBases[iBase].rHostiles[0]);
			if (oTarget) {
				for (let iTower in Memory.config.oBases[iBase].rTowers) {
					let oTower = Game.getObjectById(
						Memory.config.oBases[iBase].rTowers[iTower]
					);
					oTower.attack(oTarget);
				}
				let sUsername = "[Unknown]";
				if (oTarget) {
					if (oTarget.owner != undefined) {
						if (oTarget.owner.username != undefined) {
							sUsername = oTarget.owner.username;
						}
					}
				}
				console.log("Firing in room " + Memory.config.oBases[iBase].sRoom + "on " + sUsername);
			} else {
				Memory.config.oBases[iBase].rHostiles = m2.tools.spliceh(Memory.config.oBases[iBase].rHostiles);
			}
		} else {
			Memory.config.oBases[iBase].activeDefence = false;
			m2_tower.roomStance(m2, iBase);
		}
	},

	repair : function (m2, iBase) {

		if (Memory.config.oBases[iBase].rRepairs.length > 0) {
			let oBuilding = Game.getObjectById(Memory.config.oBases[iBase].rRepairs[0]);
			if (oBuilding) {
				let iTresh = (Memory.config.oBases[iBase].buildingHitThreshold
				> oBuilding.hitsMax
				? oBuilding.hitsMax
				: Memory.config.oBases[iBase].buildingHitThreshold);
				if (oBuilding.hits < iTresh) {
					for (iTower in Memory.config.oBases[iBase].rTowers) {
						let oTower = Game.getObjectById(Memory.config.oBases[iBase].rTowers[iTower]);
						rTrn = oTower.repair(oBuilding);
						if (oBuilding.hits >= iTresh) {
							Memory.config.oBases[iBase].rRepairs = m2.tools.spliceh(Memory.config.oBases[iBase].rRepairs);
							break ;
						}
					}
				} else {
					Memory.config.oBases[iBase].rRepairs = m2.tools.spliceh(Memory.config.oBases[iBase].rRepairs);
				}
			} else {
				Memory.config.oBases[iBase].rRepairs = m2.tools.spliceh(Memory.config.oBases[iBase].rRepairs);
			}
		} else {
			if(Game.time % 50 === 0) {
				var bRepairs = Game.rooms[Memory.config.oBases[iBase].sRoom].find(
					FIND_STRUCTURES,
					{
						filter: (structure) => {
							return (
								(
									structure.structureType ==
									STRUCTURE_RAMPART ||
									structure.structureType ==
									STRUCTURE_ROAD ||
									structure.structureType ==
									STRUCTURE_CONTAINER
								) &&
								structure.hits < (
									Memory.config.oBases[iBase].buildingHitThreshold
									> structure.hitsMax
									? structure.hitsMax
									: Memory.config.oBases[iBase].buildingHitThreshold
								)
							)
						}
					}
				);
				if (bRepairs.length > 0) {
					for (let iR in bRepairs) {
						Memory.config.oBases[iBase].rRepairs.push(bRepairs[iR].id);
					}
				}
			}
		}
	},
	
	roomStance : function (m2, iBase) {
		console.log("m2_tower: Change of stance for " + Memory.config.oBases[iBase].sRoom + ". Set to " + Memory.config.oBases[iBase].activeDefence);
		let rRamparts = Game.rooms[Memory.config.oBases[iBase].sRoom].find(
			FIND_STRUCTURES,
			{
				filter: (structure) => {
					return (
						(
							structure.structureType ==
							STRUCTURE_RAMPART
						)
					)
				}
			}
		);
		if (rRamparts.length > 0) {
			for (let iR in rRamparts) {
				if (Memory.config.oBases[iBase].activeDefence) {
					rRamparts[iR].setPublic(false);
				} else {
					rRamparts[iR].setPublic(true);
				}
			}
		}
	},
	basehook : function (m2, iBase) {
		let eLog = Game.rooms[Memory.config.oBases[iBase].sRoom].getEventLog();
		let eAttackEvent = _.filter(eLog, {event: EVENT_ATTACK});
		if (eAttackEvent.length > 0) {
			Memory.config.oBases[iBase].activeDefence = true;
			m2_tower.roomStance(m2, iBase);
			let rHostiles = Game.rooms[Memory.config.oBases[iBase].sRoom].find(
				FIND_HOSTILE_CREEPS
			);
			if (rHostiles.length > 0) {
				Memory.config.oBases[iBase].rHostiles = Array();
				for (iH in rHostiles) {
					Memory.config.oBases[iBase].rHostiles.push(rHostiles[iH].id);
				}
			}
		}
		if (Memory.config.oBases[iBase].activeDefence) {
			m2_tower.fight(m2, iBase);
		} else if (Game.cpu.bucket > 2000) {
			m2_tower.repair(m2, iBase);
		}
	}
}
module.exports = m2_tower;
