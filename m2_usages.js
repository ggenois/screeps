// m2_usages

var m2_usages = {
	sName : 'm2_usages',
	lock : false,
	arr : [],
	// get cpu bucket usage
	bar : function () {
		let out;
		let ib;
		let ir;

		out = "";
		ib = 0;
		ib = Game.cpu.bucket;
		ir = 10000 - ib;
		while ((ib = Math.round(ib - 500)) > 0)
			out = out + "&vert;";
		while ((ir = Math.round(ir - 500)) > 0)
			out = out + " ";
		if (Game.cpu.getUsed() > 20) {
			console.log("Current CPU: " + Math.round(Game.cpu.getUsed()) + " >> Bucket [" + out + "] " + Game.cpu.bucket + "/10000 (" + Game.time + ")");
		}

	},
	open : function (skey) {
		if (Memory.config == null || Memory.config == undefined) {
			return (0);
		}
		if (Memory.config.usage == null || Memory.config.usage == undefined) {
			Memory.config.usage = false;
		}
		m2_usages.lock = Memory.config.usage;
		if(m2_usages.lock)
			return (0);
		sid = Math.random().toString(36).substr(2, 9);
		var ortn = {
			key : skey,
			time : Game.time,
			cpua : Game.cpu.getUsed(),
			cpub : 0,
			cpu: 0,
			id: sid
		};
		m2_usages.arr[sid] = ortn;
		return (sid);
	},
	close : function (sid) {
		if(m2_usages.lock)
			return (0);
		if (m2_usages.arr[sid] != undefined)
 		{
			{
				m2_usages.arr[sid].cpub = Game.cpu.getUsed();
				m2_usages.arr[sid].cpu = m2_usages.arr[sid].cpub - m2_usages.arr[sid].cpua;
				return (0);
			}
		}
		return (-1);
	},
	display : function (sKey) {
		if(m2_usages.lock)
			return (0);
		var itotal = 0;
		var hits = 0;
		for (var i in m2_usages.arr)
		{
			if (m2_usages.arr[i].key == sKey)
			{
				itotal = itotal + m2_usages.arr[i].cpu;
				hits++;
			}
		}
		console.log("CPU usage for [" + sKey + "]["+hits+"] &vert;&vert;&vert;&vert; : " + (Math.round(itotal * 100) / 100));
	},
	displayAll : function () {
		if(m2_usages.lock)
			return (0);
		var rKey = Array();
		var itotal = 0;
		var hits = 0;
		for (var i in m2_usages.arr)
		{
			if (rKey[m2_usages.arr[i].key] == undefined) {
				rKey[m2_usages.arr[i].key] = {
					cpu : m2_usages.arr[i].cpu,
					hits: 1
				};
			} else {
				rKey[m2_usages.arr[i].key].cpu += m2_usages.arr[i].cpu;
				rKey[m2_usages.arr[i].key].hits++;
			}
			itotal = itotal + m2_usages.arr[i].cpu;
		}
		for (let iK in rKey) {
			console.log(rKey[iK].cpu.toFixed(2) + " [" + rKey[iK].hits + "] " + iK);
		}
		console.log("Total registered (...): " + itotal.toFixed(2));
		m2_usages.bar();
	}

};
module.exports = m2_usages;
