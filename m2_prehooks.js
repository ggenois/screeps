
var m2_prehooks = {
	sName : 'm2_prehooks',
	run : function (m2, creep) {
		let bRtn = m2_prehooks.boostcreep(m2, creep);
		if (!bRtn) {
			creep.say("Up...");
		} else {

		}
		return bRtn;
	},
	/*
		Return True:	No action, move forward
		Return False:	Should be updated.
	*/
	boostcreep : function (m2, creep) {
		
		var obase = m2.tools.getBaseInMemoryByRoom(creep.room.name);
		if (creep.memory.boosted == true || obase == null)
			return (true);
		switch (creep.memory.role) {
			case 'mover':
			case 'builder':
			case 'powers_collector':
				var sLabId = null;
				var oLab = null;
				for (var iM in obase.lreact)
				{
					if (obase.lreact[iM].id == RESOURCE_ZYNTHIUM_OXIDE)
					{
						sLabId = obase.lreact[iM].uidTo;
						break ;
					}
				}

				if (sLabId)
					oLab = Game.getObjectById(sLabId);
				if (!oLab)
					return (true);
				if (oLab.mineralAmount < 500 || oLab.energy < 200)
					return (true);
				var bcrtn = oLab.boostCreep(creep);
				if (bcrtn == ERR_NOT_FOUND)
					creep.memory.boosted = true;
			    else if (bcrtn == ERR_NOT_IN_RANGE)
			        creep.moveTo(oLab);
				break ;
			case 'miner':
			case 'mineral_harvester':
				var sLabId = null;
				var oLab = null;
				for (var iM in obase.lreact)
				{
					if (obase.lreact[iM].id == RESOURCE_UTRIUM_OXIDE)
					{
						sLabId = obase.lreact[iM].uidTo;
						break ;
					}
				}
				if (sLabId)
					oLab = Game.getObjectById(sLabId);
				if (!oLab)
					return (true);
				if (oLab.mineralAmount < 500 || oLab.energy < 200)
					return (true);
				var bcrtn = oLab.boostCreep(creep)
				if (bcrtn == ERR_NOT_IN_RANGE)
					creep.moveTo(oLab);
				else if (bcrtn == ERR_NOT_FOUND)
					creep.memory.boosted = true;
				break ;
			default:
				return (true);
				break ;
		}
		return (false);
	}
};

module.exports = m2_prehooks;
