// m2_creep_trader

var m2_creep_trader = {
	sName: 'module_creep_traders',
	sRole: 'traders',
	rMissions : [{
		spawnlock: false,
		arrRoad: ['E22N19', 'E22N20'],
		iMission: 0,
		iUnitByMission: 1,
		sMissionCode: 'A',
		sMissionName: 'Mist',
		sObserver: '5d1bd2fb7c50090fe44d76d1',
		iMineral: RESOURCE_MIST,
		sTargetId: '5dc2ed29903e8d47f0d38493',
		rCreepModel : [
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			CARRY,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE
		]
	},
	{
		spawnlock: false,
		arrRoad: ['E22N18', 'E22N17'],
		iMission: 1,
		iUnitByMission: 1,
		sMissionCode: 'Mdd',
		sMissionName: 'Energy',
		sObserver: '5cc9bbcff71a733b9e894c43',
		iMineral: RESOURCE_ENERGY,
		sTargetId: '5bbcae329099fc012e6388ee',
		rCreepModel : [
			WORK, WORK, WORK, WORK,
			CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY,
			MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE
		]
	},
	{
		spawnlock: false,
		arrRoad: ['E21N19', 'E20N19', 'E20N20'],
		iMission: 2,
		iUnitByMission: 1,
		sMissionCode: 'Mdd2',
		sMissionName: 'Mist 2',
		sObserver: '5c86fdf503c0d05f62993904',
		iMineral: RESOURCE_MIST,
		sTargetId: '5dcb8b1c6292414ac7db8c22',
		rCreepModel : [
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			CARRY,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE
		]
	},
	{
		spawnlock: false,
		arrRoad: ['E21N19', 'E20N19'],
		iMission: 3,
		iUnitByMission: 1,
		sMissionCode: 'Mdd3',
		sMissionName: 'Mist 3',
		sObserver: '5c86fdf503c0d05f62993904',
		iMineral: RESOURCE_MIST,
		sTargetId: '5df71c8fd90bdc1919e14dc1',
		rCreepModel : [
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			CARRY,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE
		]
	}
		
		
		
	],
	rUnitCount: Array(),
	fnc : function (creep, preehook, m2) {
		let oMission = m2_creep_trader.rMissions[creep.memory.iMission];
		let iRoad = oMission.arrRoad.indexOf(creep.room.name);
		switch(creep.memory.iMode) {
			case 0:
				// In route ==>
				if (creep.ticksToLive < 100)  {
					creep.suicide();
				}
				if (iRoad == oMission.arrRoad.length - 1) {
					creep.memory.iMode = 1;
					let oTarget = Game.getObjectById(creep.memory.sTargetId);
					creep.moveTo(oTarget);
				} else {
					creep.moveTo(Game.flags[oMission.arrRoad[iRoad + 1]]);
				}
				break;
			case 1:
				// Arive. Mine
				if (_.sum(creep.carry) == creep.carryCapacity) {
					creep.memory.iMode = 2;
				} else {
					if (creep.ticksToLive < 100 && _.sum(creep.carry) > 0) {
						creep.memory.iMode = 2;
					}
					let oTarget = Game.getObjectById(creep.memory.sTargetId);
					if (creep.harvest(oTarget) === ERR_NOT_IN_RANGE) {
						creep.moveTo(oTarget);
					}
				}
				break;
			case 2:
				// Full, go back <==
				if (iRoad == 0) {
					creep.memory.iMode = 3;
					creep.moveTo(creep.room.storage);
				} else {
					creep.moveTo(Game.flags[oMission.arrRoad[iRoad - 1]]);
				}
				break ;
			case 3:
				// unload
				if (_.sum(creep.carry) == 0) {
					creep.memory.iMode = 0;
				} else {
					if(creep.transfer(creep.room.storage, creep.memory.iMineral) == ERR_NOT_IN_RANGE) {
						creep.moveTo(creep.room.storage);
					}
				}
				break ;
			default:
				creep.memory.iMode = 0;
				break;
		}

	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_trader.sRole) {
			m2_creep_trader.rUnitCount.push(creep.memory.sMissionCode);
			m2_creep_trader.fnc(creep, prehook, m2);
		}
	},
	run : function (m2) {
		for (let iM in m2_creep_trader.rMissions) {
			let iCount = 0;
			let oMission = m2_creep_trader.rMissions[iM];
			for (let iUC in m2_creep_trader.rUnitCount) {
				if (m2_creep_trader.rUnitCount[iUC] == oMission.sMissionCode) {
					iCount++;
				}
			}
			if (iCount < oMission.iUnitByMission && !oMission.spawnlock) {
				let oObserver = Game.getObjectById(oMission.sObserver);
				oObserver.observeRoom(
					oMission.arrRoad[oMission.arrRoad.length - 1]
				);
				if (Game.getObjectById(oMission.sTargetId))
				{
					let iBase = m2.tools.findRoomIndexInMemory(oMission.arrRoad[0]);
					for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
						let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
						if (!oSpawn.spawning) {
							let arrBody = oMission.rCreepModel;
							let iBuildRtn = oSpawn.spawnCreep(
								arrBody,
								'Proute ' +
									iBase +
									Math.random().toString(36).substr(2, 3)
								,
								{
									memory : {
										iMode: 0,
										hook: m2_creep_trader.sName,
										role: m2_creep_trader.sRole,
										home: oMission.arrRoad[0],
										iMission: oMission.iMission,
										sMissionCode: oMission.sMissionCode,
										sMissionName: oMission.sMissionName,
										iMineral: oMission.iMineral,
										sTargetId: oMission.sTargetId
									}
								}
							);
							if (iBuildRtn == OK) {
								console.log("m2_creep_trader; Unit built in " + Memory.config.oBases[iBase].sRoom);
							} else {
								//console.log("m2_creep_trader; Cannot build in " + Memory.config.oBases[iBase].sRoom + " [" + iBuildRtn + "]");
							}
							break ;
						}
					}
				}
			}
		}
	}

}

module.exports = m2_creep_trader;
