// m2_power

var m2_power = {
	sName: 'module_power',
	bSpawnLock : false,
	creepModels : [
		{
			rTarget : Array(),
			sRole: 'power_miner',
			rBody: [
				TOUGH, TOUGH, TOUGH, TOUGH, TOUGH,
				TOUGH, TOUGH ,
				ATTACK, ATTACK, ATTACK, ATTACK, ATTACK,
				ATTACK, ATTACK, ATTACK, ATTACK, ATTACK,
				ATTACK, ATTACK, ATTACK, ATTACK, ATTACK,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE

			],
			iUnitLimit: 1
		},
		{
			rTarget : Array(),
			sRole: 'power_healer',
			rBody: [
				HEAL, HEAL, HEAL, HEAL, HEAL,
				HEAL, HEAL, HEAL, HEAL, HEAL,
				HEAL, HEAL, HEAL, HEAL, HEAL,
				HEAL, HEAL, HEAL, HEAL, HEAL,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE, MOVE
			],
			iUnitLimit: 1
		}
	],
	/*
		Miner
		7x Tough	10	70
		15x attack	80	1200
		23x move	50	1150
					========
						2420
		healer
		20x heal	250	5000
		20x move	50	1000
					========
						6000

				Total:	8420

		250	heal
		10	TOUGH
		50	MOVE
		80	ATTACK


	*/
	fnc_miner : function (creep, prehook, m2) {

		let iBase = m2.tools.findRoomIndexInMemory(creep.memory.sHome);



		if (iBase >= 0) {
			if (creep.room.name == Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom) {
				// In the right room
				let oTarget = Game.getObjectById(creep.memory.sTarget);
				if (oTarget) {
					if(creep.attack(oTarget) == ERR_NOT_IN_RANGE) {
						creep.moveTo(oTarget);
					}
				} else {
					Memory.config.oBases[iBase].oPowerEnginer = null;
					creep.suicide();
				}
			} else {
				const rRoute = Game.map.findRoute(
					creep.room, Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom
				);
				if(rRoute.length > 0) {
					const oExit = creep.pos.findClosestByRange(rRoute[0].exit);
					creep.moveTo(oExit);
				}
			}
		} else {
			// no base from home. Catastrophic event ?
		}
/*
	oPowerEnginer.sTargetId = rPowerBanks.id;
	Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom = sRoom;
	Memory.config.oBases[iBase].oPowerEnginer.iStep = 3

*/

	},
	// all it does : follow and heal.
	fnc_healer : function (creep, prehook, m2) {
		switch (creep.memory.iMode) {
			case 0: // init
				let oCreep = creep.pos.findClosestByRange(FIND_MY_CREEPS, {
					filter: (i) => (
						i.memory.sTarget == creep.memory.sTarget
					)
				});
				if (oCreep) {
					creep.memory.sCreepTarget = oCreep.id;
					creep.memory.iMode = 1;
				} else {
					// no creep yep to follow... wait... Move somewhere ?
					if (creep.room.terminal) {
						creep.moveTo(creep.room.terminal);
					} else if(creep.room.storage) {
						creep.moveTo(creep.room.storage);
					} else {
						// Still at the extration point. Should just
						// wait until an other attacker come.
					}
				}
				break;
			case 1: //
				let oCreep = Game.getObjectById(creep.memory.sCreepTarget);
				if (oCreep) {
					if (creep.heal(oCreep) == ERR_NOT_IN_RANGE) {
						creep.moveTo(oCreep);
					}
				} else {
					creep.say("oh...");
				}
				break ;
			default:
				creep.memory.iMode = 0;
				break ;
		}
	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_power.creepModels[0].sRole) {
			m2_power.creepModels[0].rTarget.push(creep.memory.sTarget);
			m2_power.fnc_miner(creep, prehook, m2);
		} else if (creep.memory.role == m2_power.creepModels[1].sRole) {
			m2_power.creepModels[1].rTarget.push(creep.memory.sTarget);
			m2_power.fnc_healer(creep, prehook, m2);
		}
	},
	basehook : function (m2, iBase) {
		if (
			Memory.config.oBases[iBase].oPowerEnginer == undefined ||
			Memory.config.oBases[iBase].oPowerEnginer == null
		) {
			Memory.config.oBases[iBase].oPowerEnginer = {
				iStep: 0,
				sTargetId: null,
				sTargetRoom : null,
				iSearchStep: 0,
				rStack : Array(),
				iCountdown: 100
			};
		}

		switch (Memory.config.oBases[iBase].oPowerEnginer.iStep) {
			case 0: // init passes only if base level is 8.
				if (Memory.config.oBases[iBase].iLvl > 7) {
					Memory.config.oBases[iBase].oPowerEnginer.iStep =
						Memory.config.oBases[iBase].oPowerEnginer.iStep + 1;
				}
				break ;
			case 1: // find a room where there is power is the proximity.
				if (
					Memory.config.oBases[iBase].sOberverId != null
				) {
					if (
						Memory.config.oBases[iBase].oPowerEnginer.iSearchStep == 0 &&
						Memory.config.oBases[iBase].oPowerEnginer.rStack.length == 0
					) {
						// first run
						const rRoomConnection = Game.map.describeExits(
							Memory.config.oBases[iBase].sRoom
						);
						for (let iR in rRoomConnection) {
							let oCoor =  {
								iX : rRoomConnection[iR].substr(1, 2),
								iY : rRoomConnection[iR].substr(4, 2)
							}
							if (oCoor.iX % 10 == 0 || oCoor.iY % 10 == 0) {
								Memory.config.oBases[iBase].oPowerEnginer.
									rStack.push(rRoomConnection[iR]);
							}
						}

						if (Memory.config.oBases[iBase].oPowerEnginer
							.rStack.length > 0
						) {
							let oObserver = Game.getObjectById(
								Memory.config.oBases[iBase].sOberverId
							);
							oObserver.observeRoom(
								Memory.config.oBases[iBase].oPowerEnginer.rStack[0]
							);
						}

					} else if (
						Memory.config.oBases[iBase].oPowerEnginer.rStack.length > 0
					) {
						let sRoom = Memory.config.oBases[iBase].oPowerEnginer.rStack[0];
						Memory.config.oBases[iBase].oPowerEnginer.rStack = m2.tools.spliceh(
							Memory.config.oBases[iBase].oPowerEnginer.rStack
						);
						// find if there is a power source.
						let rPowerBanks = Game.rooms[
							Memory.config.oBases[iBase].sRoom
						].find(FIND_STRUCTURES, {
							filter: (i) => (
								i.structureType == STRUCTURE_POWER_BANK &&
								i.ticksToDecay > 2500
							)
						});

						if (rPowerBanks.length > 0) {
							var iFoundE = false;
							for (let iSB in Memory.config.oBases) {
								if (Memory.config.oBases[iBase].oPowerEnginer.sTargetId == rPowerBanks[0].id) {
									iFoundE = true;
								}
							}
							if (!iFoundE) {
								Memory.config.oBases[iBase].oPowerEnginer.sTargetId = rPowerBanks[0].id;
								Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom = sRoom;
								Memory.config.oBases[iBase].oPowerEnginer.iStep = 3
							}
						}
					} else {
						// no more room to search. And nothing found.
						Memory.config.oBases[iBase].oPowerEnginer.iStep  = 2
					}
					Memory.config.oBases[iBase].oPowerEnginer.iSearchStep =
							Memory.config.oBases[iBase].oPowerEnginer.iSearchStep + 1;
				}
				break ;
			case 2 : // wait a countdown before trying
				if (Memory.config.oBases[iBase].oPowerEnginer.iCountdown > 0) {
					Memory.config.oBases[iBase].oPowerEnginer.iCountdown =
						Memory.config.oBases[iBase].oPowerEnginer.iCountdown - 1
				} else {
					Memory.config.oBases[iBase].oPowerEnginer = null;
				}
				break ;
			case 3: // Now, if we have a source, go on.
				let bBuildAvailable = false;
				let oBase = null;
				let oSpawn = null;
				let arrBody = null;

				if (
					Game.rooms[
						Memory.config.oBases[iBase].sRoom
					].energyAvailable > 8420
				) {
					bBuildAvailable = true;
					if (oBase == null) {
						oBase = m2.tools.getBaseInMemoryByRoom(
							Memory.config.oBases[iBase].sRoom
						);
					}
					if (oBase != null) {
						for (let iSpawnC in oBase.rSpawn) {
							oSpawn = Game.spawns[oBase.rSpawn[iSpawnC]];
							if (oSpawn.spawning) {
								bBuildAvailable = false;
							}
						}
					}
				}
				if (bBuildAvailable) {
					if (m2_power.creepModels[0].rTarget.indexOf(Memory.config.oBases[iBase].oPowerEnginer.sTargetId) < 0) {
						if (oBase == null) {
							oBase = m2.tools.getBaseInMemoryByRoom(Memory.config.oBases[iBase].sRoom);
						}
						if (oBase && m2_power.bSpawnLock) {
							for (let iSpawnA in oBase.rSpawn) {
								oSpawn = Game.spawns[oBase.rSpawn[iSpawnA]];
								if (!oSpawn.spawning) {
									arrBody = m2_power.creepModels[0].rBody;
									let iBuildRtn = oSpawn.spawnCreep(
										arrBody,

										'power_miner_' +
											Math.random().toString(36).substr(2, 9)
										,
										{
											memory : {
												hook: m2_power.sName,
												iMode: 0,
												role: m2_power.creepModels[0].sRole,
												sHome: Memory.config.oBases[iBase].sRoom,
												sDestination: Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom,
												sTarget: Memory.config.oBases[iBase].oPowerEnginer.sTargetId
											}
										}
									);
									break ;
								}
							}
						}
					}
					if (m2_power.creepModels[1].rTarget.indexOf(Memory.config.oBases[iBase].oPowerEnginer.sTargetId) < 0) {
						if (oBase == null) {
							oBase = m2.tools.getBaseInMemoryByRoom(Memory.config.oBases[iBase].sRoom);
						}
						if (oBase && m2_power.bSpawnLock) {
							for (let iSpawnB in oBase.rSpawn) {
								oSpawn = Game.spawns[oBase.rSpawn[iSpawnB]];
								if (!oSpawn.spawning) {
									arrBody = m2_power.creepModels[1].rBody;
									let iBuildRtn = oSpawn.spawnCreep(
										arrBody,

										'power_healer_' +
											Math.random().toString(36).substr(2, 9)
										,
										{
											memory : {
												hook: m2_power.sName,
												iMode: 0,
												role: m2_power.creepModels[1].sRole,
												sHome: Memory.config.oBases[iBase].sRoom,
												sDestination: Memory.config.oBases[iBase].oPowerEnginer.sTargetRoom,
												sTarget: Memory.config.oBases[iBase].oPowerEnginer.sTargetId
											}
										}
									);
									break ;
								}
							}
						}
					}
				}
				break ;
			default:
				// Error... Re-init
				Memory.config.oBases[iBase].oPowerEnginer = null;
				break ;
		}
	}
};

module.exports = m2_power;
