
var m2_tools = {
	sName : 'm2_tools',
	getFactoryByDest : function (iMineral) {
		for (let iRB in Memory.config.rRefineryBase) {
			if (Memory.config.rRefineryBase[iRB].dest == iMineral) {
				return iRB;
			}
		}
		return -1;
	},
	getFactoryBySrc : function (iMineral) {
		for (let iRB in Memory.config.rRefineryBase) {
			if (Memory.config.rRefineryBase[iRB].src == iMineral) {
				return iRB;
			}
		}
		return -1;
	},
	findRoomIndexInMemory : function (sRoom) {
		if (Memory.config) {
			if (Memory.config.oBases.length > 0) {
				for (let iBase in Memory.config.oBases) {
					if (Memory.config.oBases[iBase].sRoom == sRoom) {
						return (iBase);
					}
				}
			}
		}
		return (-1);
	},
	getBaseInMemoryByRoom : function (sRoom) {
		if (Memory.config) {
			if (Memory.config.oBases.length > 0) {
				for (let iBase in Memory.config.oBases) {
					if (Memory.config.oBases[iBase].sRoom == sRoom) {
						return (Memory.config.oBases[iBase]);
					}
				}
			}
		}
		return (null);
	},
	getStaticConfigByRoom : function (m2, sRoom) {
		for (let iR in m2.config.oBases) {
			if (m2.config.oBases[iR].sRoom == sRoom) {
				return (m2.config.oBases[iR]);
			}
		}
		return (null);
	},
	findEnergy : function (creep) {
		let	oTarget = null;
		let oaction = null;
		let iHasFound = 0;

		if (creep.memory.fe == undefined || creep.memory.fe == null) {
			creep.memory.fe = 0;
		}
		if (creep.memory.fe_seek == undefined || creep.memory.fe_seek == null) {
			creep.memory.fe_seek = 0;
		}
		if (creep.memory.fe == 0) {
			switch (creep.memory.fe_seek) {
				case 0:
					if (creep.room.storage) {
						if (
							creep.room.storage.store[RESOURCE_ENERGY]
							> creep.carryCapacity
						) {
							creep.memory.fe = 1;
						}
					}
					break ;
				case 1:
					 if (creep.room.terminal) {
						if (
					    	creep.room.terminal.store[RESOURCE_ENERGY]
					    	> creep.carryCapacity
					    ) {
					    	creep.memory.fe = 2;
					    }
					}
					break ;
				case 2:
					oTarget = creep.pos.findClosestByRange(FIND_STRUCTURES,
			    		{
							filter: (structure) => {
							    return (
							    	structure.structureType == STRUCTURE_CONTAINER
									) &&
									structure.store[RESOURCE_ENERGY]
									> creep.carryCapacity;
							}
						}
					);
					if (oTarget)
					{
						creep.memory.fe_targetid = oTarget.id;
						creep.memory.fe = 4;
					}
					break ;
				case 3:
					oTarget = creep.pos.findClosestByRange(
						FIND_DROPPED_RESOURCES,
						{
							filter: (item) => {
								return (item.energy >= creep.carryCapacity);
							}
						}
					);
					if (oTarget)
					{
						creep.memory.fe_targetid = oTarget.id;
						creep.memory.fe = 3;
					}
					break ;
				case 4:
					if (creep.memory.source) {
						var sources = Game.getObjectById(creep.memory.source);
		            	if (sources) {
		            		creep.memory.fe_targetid = creep.memory.source;
		            		creep.memory.fe = 4;
		            	}
					}
					break ;
				default:
					creep.memory.fe = 6;
					break ;
			}
			creep.memory.fe_seek = creep.memory.fe_seek + 1;
		} else {
			creep.memory.fe_seek = 0;
			switch (creep.memory.fe) {
				case 1: // storage
					oaction = creep.withdraw(
	                	Game.rooms[creep.room.name].storage, RESOURCE_ENERGY
	                );
					if(oaction == ERR_NOT_IN_RANGE) {
	                    creep.moveTo(Game.rooms[creep.room.name].storage);
	                } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
					break;
				case 2: // terminal
					oaction = creep.withdraw(
                		Game.rooms[creep.room.name].terminal, RESOURCE_ENERGY
                	);
					if(oaction == ERR_NOT_IN_RANGE) {
	                    creep.moveTo(Game.rooms[creep.room.name].terminal);
	                } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
	            case 3: // dropped
	            	oTarget = Game.getObjectById(creep.memory.fe_targetid);
	            	oaction = creep.pickup(oTarget);
	            	if(oaction == ERR_NOT_IN_RANGE) {

	                    creep.moveTo(oTarget);
	                } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
	            	break ;
	            case 4: // containers
	            	oTarget = Game.getObjectById(creep.memory.fe_targetid);
	            	oaction = creep.withdraw(
						oTarget, RESOURCE_ENERGY
					);
	            	if (oaction == ERR_NOT_IN_RANGE) {
	                    creep.moveTo(oTarget);
	                } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
	            	break ;
	            case 5: // selected sources
	            	oTarget = Game.getObjectById(creep.memory.fe_targetid);
	            	oaction = creep.harvest(oTarget);
	            	if(oaction == ERR_NOT_IN_RANGE) {
                        creep.moveTo(oTarget);
                    } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
	            	break ;
				default: // active sources
					var sources = creep.pos.findClosestByRange(
						FIND_SOURCES_ACTIVE
					);
					oaction = creep.harvest(sources);
			        if(oaction == ERR_NOT_IN_RANGE) {
			            creep.moveTo(sources);
			        } else if(oaction != OK) {
	                	creep.memory.fe = 0;
	                }
					break ;
			}
		}
	},
	mv : function (creep, oTarget) {
		if (!creep.memory.mp || !creep.memory.mi || creep.memory.mo != oTarget.id) {
			creep.memory.mo = oTarget.id;
			creep.memory.mp = creep.pos.findPathTo(oTarget);
			creep.memory.ms = creep.memory.mp.length;
			creep.memory.mi = 1;
		}
		creep.memory.mi++;
		if (creep.memory.mi > creep.memory.ms) {
			creep.memory.mo = oTarget.id;
			creep.memory.mp = creep.pos.findPathTo(oTarget);
			creep.memory.ms = creep.memory.mp.length + 2;
			creep.memory.mi = 1;
		} else {
			return (creep.moveByPath(creep.memory.mp));
		}
	},
	spliceh : function (arrItem) {
		let arrNew;
		let i;

		arrNew = Array();
		i = 0;
		while (++i < arrItem.length)
			if (arrItem[i] !== null)
				arrNew.push(arrItem[i]);
		return (arrNew);

	},
	splicei : function (arrItem, iIndex) {
		let arrNew;
		let i;

		arrNew = Array();
		i = -1;
		while (++i < arrItem.length)
			if (arrItem[i] !== null && iIndex != i)
				arrNew.push(arrItem[i]);
		return (arrNew);
	}
};

module.exports = m2_tools;
