// m2_build.js

var m2_build = {
	sName: 'module_build',

	isInGrid : function(m2, creep) {
		for (let iBuild in Memory.config.oBuildManaged) {
			if (Memory.config.oBuildManaged[iBuild].sRoom == creep.room.name) {
				if(
					creep.pos.x >= m2_build.getx(
						Memory.config.oBuildManaged[iBuild].iX
					) &&
					creep.pos.x <= m2_build.getx(
						Memory.config.oBuildManaged[iBuild].iX
					) + 10 &&
					creep.pos.y >= m2_build.gety(
						Memory.config.oBuildManaged[iBuild].iY
					) &&
					creep.pos.y <= m2_build.gety(
						Memory.config.oBuildManaged[iBuild].iY
					) + 12
				) {
					return (true);
				}
			}
		}
		return (false);
	},

	drawBase : function (m2, iRoomLevel, sRoom, iXRef, iYRef) {
		oBaseData = m2.config.arrBaseBuildingModels[iRoomLevel];


		for (let iC in oBaseData.arrWiredFrame) {

			let ifX = m2_build.getx(iXRef) + oBaseData.arrWiredFrame[iC].x;
			let ifY = m2_build.gety(iYRef) + oBaseData.arrWiredFrame[iC].y;

			let rFound = Game.rooms[sRoom].lookForAt(LOOK_STRUCTURES, ifX, ifY);
			if (rFound.length == 0) {
				rFound = Game.rooms[sRoom].lookForAt(LOOK_CONSTRUCTION_SITES, ifX, ifY);
			}
			if (rFound.length == 0) {
				Game.rooms[sRoom].createConstructionSite(
					ifX,
					ifY,
					oBaseData.arrWiredFrame[iC].t
				);
			}
		}
	},
	getx : function (xRef) {
		return (25 - 5 + xRef);
	},
	gety : function (yRef) {
		return (25 - 6 + yRef);
	},
	run : function (m2) {
		for (let iBuild in Memory.config.oBuildManaged) {
			let iRoomLevel = -1;
			iRoomLevel = Game.rooms[Memory.config.oBuildManaged[iBuild].sRoom].controller.level;
			if (iRoomLevel < 0) {
				iRoomLevel = 1;
			}
			if (Game.time % 10 === 0) {
				m2_build.drawBase(
					m2,
					iRoomLevel,
					Memory.config.oBuildManaged[iBuild].sRoom,
					Memory.config.oBuildManaged[iBuild].iX,
					Memory.config.oBuildManaged[iBuild].iY
				);
			}

		}
	}


};

module.exports = m2_build;
