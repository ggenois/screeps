// m2_factory

var m2_factory = {
	sName : 'm2_factory',
	basehook : function (m2, iBase) {
		if (Memory.config.oBases[iBase].sFactoryId == undefined || Memory.config.oBases[iBase].sFactoryId == null) {
			let rFactory = Game.rooms[Memory.config.oBases[iBase].sRoom].find(FIND_STRUCTURES, {
				filter: (i) => i.structureType == STRUCTURE_FACTORY
			});
			if (rFactory.length > 0) {
				Memory.config.oBases[iBase].sFactoryId = rFactory[0].id;
			} else {
				Memory.config.oBases[iBase].sFactoryId = "none";
			}
		}
		if (Memory.config.oBases[iBase].iFactoryMode == undefined || Memory.config.oBases[iBase].iFactoryMode == null) {
			Memory.config.oBases[iBase].iFactoryMode = 0;
		}
		switch (Memory.config.oBases[iBase].iFactoryMode) {
			case 0:	// compress stuff
				if (Memory.config.oBases[iBase].sFactoryId !== "none") {
					let oFactory = Game.getObjectById(Memory.config.oBases[iBase].sFactoryId);
					if (oFactory) {
						let iRB = m2.tools.getFactoryBySrc(Memory.config.oBases[iBase].iMineralType);
						if (iRB != -1) {
							let oRB = Memory.config.rRefineryBase[iRB];
							if (
								oFactory.store[oRB.src] >= oRB.srcQty && 
								oFactory.store[RESOURCE_ENERGY] >= oRB.iEnergy
							) {
								// enough energy; execute ?
								if (oFactory.cooldown == 0) {
									let iFactoryRtn = oFactory.produce(oRB.dest);
									//console.log("Factory: " + iFactoryRtn);
								}
							} else {
								// not enough ressources
								//console.log("Factory: not enough ressources in " + Memory.config.oBases[iBase].sRoom);
							}
						} else {
							//console.log("Factory: No rRefineryBase defined for the specified base mineral");
							// No rRefineryBase defined for the specified base mineral
						}
					} else {
						//console.log("Factory: Factory ID in memory is invalid...");
						// Factory ID in memory is invalid...
					}
				} else {
					//console.log("Factory: no factory in room...");
					// no factory in room...
				}
				break ;
			default:
				Memory.config.oBases[iBase].iFactoryMode = 0;
				break ;
		}
	},
	
	init : function (m2) {

		if (Memory.config.rRefineryBase == undefined || Memory.config.rRefineryBase == null) {
			Memory.config.rRefineryBase = Array();
			console.log("m2 -> Refinery : Init base ref index..");
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_REDUCTANT,
				destQty : 100,
				src: RESOURCE_HYDROGEN,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_OXIDANT,
				destQty : 100,
				src: RESOURCE_OXYGEN,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_UTRIUM_BAR,
				destQty : 100,
				src: RESOURCE_UTRIUM,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_LEMERGIUM_BAR,
				destQty : 100,
				src: RESOURCE_LEMERGIUM,
				srcQty: 500,
				iEnergy : 200
			});

			Memory.config.rRefineryBase.push({
				dest : RESOURCE_KEANIUM_BAR,
				destQty : 100,
				src: RESOURCE_KEANIUM,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_ZYNTHIUM_BAR,
				destQty : 100,
				src: RESOURCE_ZYNTHIUM,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_PURIFIER,
				destQty : 100,
				src: RESOURCE_CATALYST,
				srcQty: 500,
				iEnergy : 200
			});
			
			Memory.config.rRefineryBase.push({
				dest : RESOURCE_GHODIUM_MELT,
				destQty : 100,
				src: RESOURCE_GHODIUM,
				srcQty: 500,
				iEnergy : 200
			});

			console.log("m2 -> Refinery : ..done");
		}
	},
	

	run : function (m2) {
		m2_factory.init(m2);
	}
};

module.exports = m2_factory;

/*

RESOURCE_HYDROGEN: "H",
    RESOURCE_OXYGEN: "O",
    RESOURCE_UTRIUM: "U",
    RESOURCE_LEMERGIUM: "L",
    RESOURCE_KEANIUM: "K",
    RESOURCE_ZYNTHIUM: "Z",
    RESOURCE_CATALYST: "X",
    RESOURCE_GHODIUM: "G",

RESOURCE_UTRIUM_BAR: 'utrium_bar',
    RESOURCE_LEMERGIUM_BAR: 'lemergium_bar',
    RESOURCE_ZYNTHIUM_BAR: 'zynthium_bar',
    RESOURCE_KEANIUM_BAR: 'keanium_bar',
    RESOURCE_GHODIUM_MELT: 'ghodium_melt',
    RESOURCE_OXIDANT: 'oxidant',
    RESOURCE_REDUCTANT: 'reductant',
    RESOURCE_PURIFIER: 'purifier',
    RESOURCE_BATTERY: 'battery',

*/
