/*
	Memory enabled hive. v2.
*/
var m2 = {
	tools : null,			// set of tools available globaly
	config: null,			// Static config to be loaded into memory
	init: null,				// Init module link.
	prehooks: null,			// Prehook module link.
	usages : null,			// usages tools
	arrModules : Array(),	// Modules hooks and functions
	arrBaseHooks : Array(),	// Manual base hooks
	
	// Load and link files to m2.
	load_files : function (m2) {
		m2.config		= require("m2_config");
		m2.tools		= require("m2_tools");
		m2.prehooks		= require("m2_prehooks");
		m2.init			= require("m2_init");
		m2.usages		= require("m2_usages");
		
		m2.arrModules["module_creep_basic"]		= require("m2_creep_basic");
		m2.arrModules["module_creep_mover"]		= require("m2_creep_mover");
		m2.arrModules["module_creep_updater"]	= require("m2_creep_updater");
		m2.arrModules["module_creep_energy"]	= require("m2_creep_energy");
		m2.arrModules["module_creep_mineral"]	= require("m2_creep_mineral");
		m2.arrModules["module_creep_claim"]		= require("m2_creep_claim");
		m2.arrModules["module_creep_traders"]	= require("m2_creep_trader");
		m2.arrModules["module_creep_builder"]	= require("m2_creep_builder");
		m2.arrModules["module_creeps_status"]	= require("m2_creeps_status");
		m2.arrModules["module_creep_power"]		= require("m2_creep_power");
		m2.arrModules["module_tower"]			= require("m2_tower");
		m2.arrModules["module_market"]			= require("m2_market");
		m2.arrModules["module_factory"]			= require("m2_factory");
		m2.arrModules["module_lab"]				= require("m2_lab");
		m2.arrModules["module_federation"]		= require("m2_federation");
		m2.arrModules["module_prospector"]		= require("m2_prospector");
		m2.arrModules["module_bootstrap"]		= require("m2_bootstrap");
		m2.arrModules["module_build"]			= require("m2_build");
		//m2.arrModules["module_power"]			= require("m2_power");
		
	},

	// Parse every creep and load appropriate hook function for it.
	creepsHooks : function () {
		for (let iUnit in Game.creeps) {
			let oUnit = Game.creeps[iUnit];
			if (oUnit.hits < oUnit.hitsMax) {
				const target = oUnit.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
				if(target) {
					Memory.config.blacklist.push(target.owner.username);
				}
			}
			try {
				if (m2.arrModules[oUnit.memory.hook] != undefined) {
					let chook = m2.arrModules[oUnit.memory.hook].creephook;
					if (chook != undefined) {
						let usid = m2.usages.open(oUnit.memory.hook);
						let hres = chook(oUnit, m2.prehooks.run(m2, oUnit), m2);
						m2.usages.close(usid);
					} else {
						console.log("m2: " + oUnit.memory.hook + " does not have a creephook");
					}
				} else {
					console.log("m2: Creep with undefined hook.");
				}
			} catch (e) {
				console.log(
					"Hook error for " +
					oUnit.memory.hook +
					"[In:" +
					oUnit.memory.home +
					" Currently in "+
					oUnit.room.name +
					"](" + oUnit.name + "):" + e);
			}
		}
	},
	
	// Run scripts for every bases
	baseFncs : function () {
		for (let iC in m2.arrModules) {
			if (m2.arrModules[iC].run != undefined) {
				try {
					let usid = m2.usages.open(m2.arrModules[iC].sName);
					let cRun = m2.arrModules[iC].run;
					let hres = cRun(m2);
					m2.usages.close(usid);
				} catch (e) {
					console.log("Run error for "+m2.arrModules[iC].run+":" + e);
				}
			}
			if (m2.arrModules[iC].basehook != undefined) {
				try {
					let oBaseHook = m2.arrModules[iC].basehook;
					for (let iM in Memory.config.oBases) {
						oBaseHook(m2, iM);
					}
				} catch (e) {
					console.log("Run error for " + iC + ":" + e);
				}
			}
		}
	},
	
	// Main entry point to this module (m2).
	run : function () {
		m2.load_files(m2);
		m2.init.run(m2);

		m2.creepsHooks();	// Run correct module for each creep.
		m2.baseFncs();		// Run module base scrips. For hooks and runs.

		m2.usages.bar();
	}
};

module.exports = m2;
