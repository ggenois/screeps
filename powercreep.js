
var powercreep = {
	findpower : function (creep) {
		if (creep.room.storage.store[RESOURCE_POWER])
		{
			if (creep.room.storage.store[RESOURCE_POWER] >= creep.carryCapacity)
			{
				var storp = creep.withdraw(creep.room.storage, RESOURCE_POWER);
				console.log(storp);
				if(storp == ERR_NOT_IN_RANGE)
					creep.moveTo(creep.room.storage);
			}
		}
		if (creep.room.terminal.store[RESOURCE_POWER])
		{
			if (creep.room.terminal.store[RESOURCE_POWER] >= creep.carryCapacity)
			{
				var termp = creep.withdraw(creep.room.terminal, RESOURCE_POWER);
				console.log(termp);
				if(termp == ERR_NOT_IN_RANGE)
					creep.moveTo(creep.room.terminal);
			}
		}
	},
	cycle : function () {
		var oHappy = Game.powerCreeps['happy'];
		var sStartRoom = "";
		var missions = [
			{
				sRoom: "E21N19",
				arrSources : ['5bbcae249099fc012e638760','5bbcae249099fc012e638762'],
				spowerspawn : '5c879b015c7332130daf6153'
			},
			{
				sRoom: "E21N18",
				arrSources : ['5bbcae249099fc012e638765','5bbcae249099fc012e638766'],
				spowerspawn : '5cb50a32f10dfb205d9f51f9'
			},
			{
				sRoom: "E22N18",
				arrSources : ['5bbcae329099fc012e6388ea','5bbcae329099fc012e6388ec'],
				spowerspawn : '5cc97e5e1a3ca4284d56da5e'
			}
		];
		var oAction;
		if (oHappy.memory.iCycle == null || oHappy.memory.iCycle == undefined)
		{
			oHappy.memory.iCycleStep = 0;
			oHappy.memory.iCycle = 0;
		}
			
		
		if (oHappy.room.name == missions[oHappy.memory.iCycle].sRoom)
		{
			if (oHappy.powers[PWR_GENERATE_OPS].cooldown == 0)
				oHappy.usePower(PWR_GENERATE_OPS);
			var oPSpawn = Game.getObjectById(missions[oHappy.memory.iCycle].spowerspawn);
			if (oHappy.ticksToLive < 3000)
			{
				if (oHappy.renew(oPSpawn) == ERR_NOT_IN_RANGE)
					oHappy.moveTo(oPSpawn);
			} else {
				if (oHappy.memory.iCycleStep == 0 || oHappy.memory.iCycleStep == undefined || oHappy.memory.iCycleStep == null)
				{
					oAction = oHappy.renew(oPSpawn);
					if (oAction == ERR_NOT_IN_RANGE)
						oHappy.moveTo(oPSpawn);
					else if (oAction == OK)
					{
						oHappy.memory.iCycleStep = 1;
						oHappy.memory.iCycleSource = 0;
					}
				}
				if (oHappy.memory.iCycleStep == 1)
				{
					if (oHappy.carry[RESOURCE_OPS] > 1000)
					{
						var rtnp = oHappy.transfer(oHappy.room.storage, RESOURCE_OPS, 750);
						if (rtnp == ERR_NOT_IN_RANGE) {
							oHappy.moveTo(oHappy.room.storage);
						}
					}
					else if (oHappy.powers[PWR_REGEN_SOURCE].cooldown == 0)
					{
						var oSource = Game.getObjectById(missions[oHappy.memory.iCycle].arrSources[oHappy.memory.iCycleSource]);
						oAction = oHappy.usePower(PWR_REGEN_SOURCE, oSource);
						if (oAction == OK) {
							oHappy.memory.iCycleSource++;
							if (oHappy.memory.iCycleSource >= missions[oHappy.memory.iCycle].arrSources.length)
							{
								oHappy.memory.iCycleSource = 0;
								oHappy.memory.iCycleStep = 2;
							}
						} else if (oAction == -10)
						{
							if(oHappy.enableRoom(
								oHappy.room.controller
							) == ERR_NOT_IN_RANGE)
							{
								oHappy.moveTo(oHappy.room.controller);
							}
						} else if (oAction == ERR_NOT_IN_RANGE)
						{
							oHappy.moveTo(oSource);
						}
					}
					else
					{
						oHappy.moveTo(Game.flags[oHappy.room.name]);
					}
				}
				if (oHappy.memory.iCycleStep == 2)
				{
					oAction = oHappy.renew(oPSpawn);
					if (oAction == ERR_NOT_IN_RANGE)
						oHappy.moveTo(oPSpawn);
					else if (oAction == OK)
					{
						oHappy.memory.iCycleStep = 0;
						oHappy.memory.iCycle++;
						if (missions.length <= oHappy.memory.iCycle)
						{
							oHappy.memory.iCycle = 0;
						}
					}
					
				}
			}
		} else {
			oHappy.moveTo(Game.flags[missions[oHappy.memory.iCycle].sRoom]);
		}
	},
	
	run : function () {
	    
		var oPSpawn = Game.getObjectById('5c879b015c7332130daf6153');
		var arrPSpawn = [
		    '5c879b015c7332130daf6153',
		    '5ca693dc524cfa55fb2d8b4c',
		    '5cb50a32f10dfb205d9f51f9',
		    '5cc97e5e1a3ca4284d56da5e',
		    '5cdd133187e2ff2131ef3b09' 
		];
		
		var oHappy = Game.powerCreeps['happy'];
		if (oHappy.room)
		{
			powercreep.cycle();
		}
		else
		{
			Game.powerCreeps['happy'].spawn(oPSpawn);
		}
		
		for (var iPS in arrPSpawn)
		{
		    var oP = Game.getObjectById(arrPSpawn[iPS]);
		    if (oP.power > 0)
			    oP.processPower();
		    
		}
		
	}
};

module.exports = powercreep;
