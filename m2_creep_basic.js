// m2_creep_basic

var m2_creep_basic = {
	sName : 'module_creep_basic',
	sRole : 'basic',
	bSpawnLock : false,
	arrUnitsLimits : [0,2,2,2,2,2,2,2,2],
	arrBodies: [
		[ null ],
		[WORK, MOVE, CARRY], // lvl 1
		[WORK, MOVE, CARRY], // lvl 2
		[WORK, MOVE, CARRY], // lvl 3
		[WORK, MOVE, CARRY], // lvl 4
		[WORK, MOVE, CARRY], // lvl 5
		[WORK, MOVE, CARRY], // lvl 6
		[WORK, MOVE, CARRY], // lvl 7
		[WORK, MOVE, CARRY], // lvl 8
		[WORK, MOVE, CARRY] // lvl 9 - Powers
	],
	arrUnitsByRoom : Array(),
	fnc : function (creep, prehook, m2) {
		let	oTarget = null;
		let rTagets = [];
		let oaction = null;
		if(creep.carry.energy == 0) {
		   creep.memory.iMode = 0;
		}
		if(creep.memory.iMode == 0 && creep.carryCapacity == creep.carry.energy) {
			creep.memory.fe = 0;
			rTagets = creep.room.find(FIND_MY_STRUCTURES, {
		        filter: (structure) => {
		            return (
		    			structure.structureType ==
		    			STRUCTURE_EXTENSION ||
		    			structure.structureType ==
		    			STRUCTURE_SPAWN
		            ) &&
		            (structure.energy < structure.energyCapacity);
		        }
		    });
		    if(rTagets.length > 0) {
		    	creep.memory.targetId = rTagets[0].id;
		    	creep.memory.iMode = 1;
		    } else {
		    	rTagets = creep.room.find(FIND_MY_STRUCTURES, {
		            filter: (structure) => {
		                return (
		                	structure.structureType
		                	== STRUCTURE_TOWER) &&
		                	(structure.energy <=
		                	(structure.energyCapacity - 50));
		            }
		        });
		        if(rTagets.length > 0) {
		        	creep.memory.targetId = rTagets[0].id;
		    		creep.memory.iMode = 2;
		        } else {
		        	creep.memory.iMode = 3;
		        }
		    }
		}

		switch (creep.memory.iMode) {
			case 0:
				m2.tools.findEnergy(creep);
				break;
			case 1:
				oTarget = Game.getObjectById(creep.memory.targetId);
				oaction = creep.transfer(oTarget, RESOURCE_ENERGY);
				if(oaction == ERR_NOT_IN_RANGE) {
		            creep.moveTo(oTarget);
		        } else if(oaction != OK) {
		        	creep.memory.iMode = 0;
		        }
		    case 2:
		    	oTarget = Game.getObjectById(creep.memory.targetId);
				oaction = creep.transfer(oTarget, RESOURCE_ENERGY);
				if(oaction == ERR_NOT_IN_RANGE) {
		            creep.moveTo(oTarget);
		        } else if(oaction != OK) {
		        	creep.memory.iMode = 0;
		        }
		    	break ;
		    default:
		    	if(
	        		creep.upgradeController(
	        			creep.room.controller
	        		) == ERR_NOT_IN_RANGE
	        	) {
	                creep.moveTo(creep.room.controller);
	            }
				break ;
		}
	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_basic.sRole) {
			m2_creep_basic.arrUnitsByRoom.push(creep.memory.home);
			m2_creep_basic.fnc(creep, prehook, m2);
		}
	},
	
	basehook : function (m2, iBase) {
		let iCount = 0;
		let iLimit = m2_creep_basic.arrUnitsLimits[Memory.config.oBases[iBase].iLvl];
		for (let iB in m2_creep_basic.arrUnitsByRoom) {
			if (m2_creep_basic.arrUnitsByRoom[iB] == Memory.config.oBases[iBase].sRoom) {
				iCount++;
			}
		}
		if (iLimit - iCount > 0 && (Memory.arrBasicStatus[iBase] || Memory.config.oBases[iBase].iLvl < 4)) {
			for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
				let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
				if (!oSpawn.spawning && !m2_creep_basic.bSpawnLock) {
					let arrBody = m2_creep_basic.arrBodies[Memory.config.oBases[iBase].iLvl];
					let iBuildRtn = oSpawn.spawnCreep(
						arrBody,
						m2_creep_basic.sRole +
							'[' +
							iBase +
							']_' +
							Math.random().toString(36).substr(2, 9)
						,
						{
							memory : {
								hook: m2_creep_basic.sName,
								iMode: 0,
								role: m2_creep_basic.sRole,
								home: Memory.config.oBases[iBase].sRoom
							}
						}
					);
					if (iBuildRtn == OK) {
						console.log("m2_creep_basic; Unit built in " + Memory.config.oBases[iBase].sRoom);
					} else if(iBuildRtn == ERR_NOT_ENOUGH_ENERGY) {
						console.log("m2_creep_basic; No enough energy to build creep in [" + Memory.config.oBases[iBase].sRoom + "]");
					}

					break ;
				}
			}
		}
	}
}

module.exports = m2_creep_basic;
