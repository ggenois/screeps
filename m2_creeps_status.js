// m2_creep_status

var m2_creeps_status = {
	sName : 'm2_creeps_status',
	minerals : function(m2, i) {
		if (
            Memory.config.oBases[i].bMiner == undefined || Memory.config.oBases[i].bMiner == null
        ) {
            Memory.config.oBases[i].bMiner = false;
        }
			
			
        if (
            Memory.config.oBases[i].arrMineralSource.length > 0
        ) {
            let oSource = Game.getObjectById(Memory.config.oBases[i].arrMineralSource[0]);
            if (
                oSource
            ) {
                if (
                    oSource.density
                ) {
                    if (oSource.mineralAmount > 0) {
                        Memory.config.oBases[i].bMiner = true;
                    } else {
                        Memory.config.oBases[i].bMiner = false;
                    }
                }
            }
        }
	},
    
    builders : function (m2, i) {
        if
        (
            Memory.config.oBases[i].bBuilder == undefined || Memory.config.oBases[i].bBuilder == null
        ) {
            Memory.config.oBases[i].bBuilder = false;
        }
        let rTargets = Game.rooms[Memory.config.oBases[i].sRoom].find(
            FIND_CONSTRUCTION_SITES
        );
        Memory.config.oBases[i].bBuilder = false;
        if (rTargets.length > 0) {
            Memory.config.oBases[i].bBuilder = true;
        } else {
            var iCare = Memory.config.oBases[i].buildingHitThreshold - 900;
            rTargets = Game.rooms[Memory.config.oBases[i].sRoom].find(FIND_STRUCTURES,
                {
                    filter: (structure) => {
                        return (
                            (
                                structure.structureType ==
                                STRUCTURE_WALL
                            ) &&
                            structure.hits < (
                                iCare
                                > structure.hitsMax
                                    ? structure.hitsMax
                                    : iCare
                            )
                        )
                    }
                }
            );
            if (rTargets.length > 0) {
                Memory.config.oBases[i].bBuilder = true;
            } else {
            	if (Game.rooms[Memory.config.oBases[i].sRoom].storage) {
            		if (Game.rooms[Memory.config.oBases[i].sRoom].storage.store[RESOURCE_ENERGY] > 700000) {
		                Memory.config.oBases[i].bBuilder = true;
		            }
            	}

            }
        }
    },
    
	basehook : function (m2, iBase) {
		m2_creeps_status.minerals(m2, iBase);
		if (Game.time % 10 === 0) {
			m2_creeps_status.builders(m2, iBase);
		}
	}
};


module.exports = m2_creeps_status;
