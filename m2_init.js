// m2_init
// prise 2
var m2_init = {
	sName : 'm2_init',
	run : function (m2) {
		let usid = m2.usages.open("init");
		if (Memory.config == null || Memory.config == undefined) {
			console.log("Memory init from scratch...");
			Memory.config = {
				init: true,
				oBases: Array(),
				masterlock: false,
				refresh : null,
				sRoomSign: 'c01 was here!. Peaceful player. Love to trade.'
			};
		}
		if (Memory.config.blacklist == undefined || Memory.config.blacklist == null) {
			Memory.config.blacklist = Array();
		}
		if (
			Memory.config.refresh == undefined ||
			Memory.config.refresh == null
		) {
			console.log("Memory refresh...");
			var arrSpawnsx = Array();
			for (let iX in Memory.config.oBases) {
				console.log("Validating Existing spawns for ["+ Memory.config.oBases[iX].sName +"]...");

				for (let iXX in Memory.config.oBases[iX].rSpawn) {
					arrSpawnsx.push(Memory.config.oBases[iX].rSpawn[iXX]);
					let oSpawntc = Game.spawns[
						Memory.config.oBases[iX].rSpawn[iXX]
					];
					if (oSpawntc == undefined) {
						console.log("REMOVING SPAWN ["+Memory.config.oBases[iX].rSpawn[iXX]+"]");
						Memory.config.oBases[iX].rSpawn = m2.tools.splicei(Memory.config.oBases[iX].rSpawn, iXX);
					} else {
						console.log(
							Memory.config.oBases[iX].rSpawn[iXX] +
							" ... valid"
						);
					}
				}

				console.log("Refreshing static config...");
				let oBaseConfig = m2.tools.getStaticConfigByRoom(
					m2, Memory.config.oBases[iX].sRoom
				);
				if (oBaseConfig !== null) {
					console.log("Config found. Aligning...");
					Memory.config.oBases[iX].sName = oBaseConfig.sName;
					Memory.config.oBases[iX].labs = oBaseConfig.labs;
					Memory.config.oBases[iX].lreact = oBaseConfig.lreact;
				} else {
					console.log("[W] (Refresh) m2.init.run => No static config found for room " + Memory.config.oBases[iX].sRoom);
				}
				console.log("Aligning mineral type (bug correction)");
				let oMinerals = Game.rooms[Memory.config.oBases[iX].sRoom].find(FIND_MINERALS);
				if (oMinerals) {
					for (let iMI in oMinerals) {
						console.log("["+ oMinerals[iMI].mineralType +"]");
						Memory.config.oBases[iX].iMineralType = oMinerals[iMI].mineralType;
					}
				}
				console.log (Memory.config.oBases[iX].iMineralType + " Found.");
			}

			console.log("Looking for new spawn...");
			for (let iSpawn in Game.spawns) {
				if (arrSpawnsx.indexOf(Game.spawns[iSpawn].name) < 0) {
					console.log("New spawn ["+Game.spawns[iSpawn].name+"] detected. Addind...");
					let iSpawnBase = m2.tools.findRoomIndexInMemory(
						Game.spawns[iSpawn].room.name
					);
					if (iSpawnBase < 0) {
						console.log("No previous room. New base. Adding...");
						m2_init.add_base(m2, Game.spawns[iSpawn].room.name);
						let iNewSpawnBase = m2.tools.findRoomIndexInMemory(
							Game.spawns[iSpawn].room.name
						);
						if (iNewSpawnBase < 0) {
							console.log("Adding new spawn to new base");
							Memory.config.oBases[iNewSpawnBase].rSpawn.push(
								Game.spawns[iSpawn].name
							);
						} else {
							console.log("[E] Unable to push Spawn to new base. Should look into that...");
						}

					} else {
						console.log("Room ["+Memory.config.oBases[iSpawnBase].sRoom+"]found. Adding spawn ["+Game.spawns[iSpawn].name+"] to it.");
						Memory.config.oBases[iSpawnBase].rSpawn.push(
							Game.spawns[iSpawn].name
						);
					}
				}
			}

			console.log("Updating base level...");

			for (let iBaseUpdate in Memory.config.oBases) {
				if (
					Memory.config.oBases[iBaseUpdate].iLvl !=
					Game.rooms[
						Memory.config.oBases[iBaseUpdate].sRoom
					].controller.level
				) {
					console.log("Room level changed for " + Memory.config.oBases[iBaseUpdate].sRoom);
					if (
						m2.config.arrEnergyByLevel[Game.rooms[
							Memory.config.oBases[iBaseUpdate].sRoom
						].controller.level] >=
						Game.rooms[
							Memory.config.oBases[iBaseUpdate].sRoom
						].energyCapacityAvailable
					) {
						console.log ("Enough energy to lvlup...");
						console.log(
							m2.config.arrEnergyByLevel[Game.rooms[
								Memory.config.oBases[iBaseUpdate].sRoom
							].controller.level] + "/" +
							Game.rooms[
								Memory.config.oBases[iBaseUpdate].sRoom
							].energyCapacityAvailable
						);
						Memory.config.oBases[iBaseUpdate].iLvl = Game.rooms[
							Memory.config.oBases[iBaseUpdate].sRoom
						].controller.level;
					} else {
						console.log("Waiting... " +
							m2.config.arrEnergyByLevel[Game.rooms[
								Memory.config.oBases[iBaseUpdate].sRoom
							].controller.level] + "/" +
							Game.rooms[
								Memory.config.oBases[iBaseUpdate].sRoom
							].energyCapacityAvailable
						);
					}

				} else {
					console.log("Room level did not change for " + Memory.config.oBases[iBaseUpdate].sRoom);
					console.log(Game.rooms[Memory.config.oBases[iBaseUpdate].sRoom].energyCapacityAvailable);
				}
				//Memory.config.oBases[iBaseUpdate].iLvl

				if (
					Memory.config.oBases[iBaseUpdate].sOberverId == undefined ||
					Memory.config.oBases[iBaseUpdate].sOberverId == null
				) {
					if (Memory.config.oBases[iBaseUpdate].rSpawn.length > 0) {
						let rObservers = Game.spawns[Memory.config.oBases[iBaseUpdate].rSpawn[0]].room.find(FIND_MY_STRUCTURES, {
							filter: { structureType: STRUCTURE_OBSERVER }
						});
						if (rObservers.length > 0) {
							Memory.config.oBases[iBaseUpdate].sOberverId = rObservers[0].id;
						} else {
							Memory.config.oBases[iBaseUpdate].sOberverId = null;
						}
					}


				}

				// Update Towers
				let rTowers = Game.rooms[Memory.config.oBases[iBaseUpdate].sRoom].find(
					FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}}
				);
				Memory.config.oBases[iBaseUpdate].rTowers = Array();
				if (rTowers.length > 0) {
					for (let iTowers in rTowers) {
						Memory.config.oBases[iBaseUpdate].rTowers.push(rTowers[iTowers].id);
					}
				}

			}

			//Game.rooms[Memory.config.oBases[iBaseUpdate].sRoom].controller.level






			Memory.config.refresh = true;
			console.log("Memory refresh done");
		}

		if (Memory.config.init) {
			console.log("Memory init launched...");
			Memory.config.oBases = Array();
			for (let iSpawn in Game.spawns) {
				if (Game.spawns[iSpawn].my) {
					let iBase = m2.tools.findRoomIndexInMemory(
						Game.spawns[iSpawn].room.name
					);
					if (iBase < 0) {
						Memory.config.oBases.push({
							sName: Math.random().toString(36).substr(2, 9),
							sRoom: Game.spawns[iSpawn].room.name,
							energyCapacityAvailable: Game.spawns[iSpawn].room.energyCapacityAvailable,
							rSpawn: Array(),
							iLvl: Game.rooms[Game.spawns[iSpawn].room.name].controller.level,
							arrEnergySources: Array(),
							bBuilder: false,
							bMiner : false,
							arrMineralSource: Array(),
							iMineralType : 0,
							sPowerSpawn: null,
							buildingHitThreshold: 10000,
							labs: Array(),
							lreact: Array()
						});
						iBase = m2.tools.findRoomIndexInMemory(Game.spawns[iSpawn].room.name);
					}
					Memory.config.oBases[iBase].rSpawn.push(Game.spawns[iSpawn].name);
				}
			}
			for (let iB in Memory.config.oBases) {
				let oSources = Game.rooms[Memory.config.oBases[iB].sRoom].find(FIND_SOURCES);
				if (oSources) {
					for (let iS in oSources) {
						Memory.config.oBases[iB].arrEnergySources.push(oSources[iS].id);
					}
				}
				let oMinerals = Game.rooms[Memory.config.oBases[iB].sRoom].find(FIND_MINERALS);
				if (oMinerals) {
					for (let iMI in oMinerals) {
						Memory.config.oBases[iB].arrMineralSource.push(oMinerals[iMI].id);
						Memory.config.oBases[iB].iMineralType = oMinerals[iMI].mineralType;
					}
				}
				let oBaseConfig = m2.tools.getStaticConfigByRoom(
					m2, Memory.config.oBases[iB].sRoom
				);
				if (oBaseConfig !== null) {
					Memory.config.oBases[iB].sName = oBaseConfig.sName;
					Memory.config.oBases[iB].labs = oBaseConfig.labs;
					Memory.config.oBases[iB].lreact = oBaseConfig.lreact;
				} else {
					console.log("[W] m2.init.run => No static config found for room " + Memory.config.oBases[iB].sRoom);
				}
			}
			console.log("Memory init done.");
			Memory.config.init = false;
		}
		if (Memory.arrBasicStatus == null || Memory.arrBasicStatus == undefined) {
			Memory.arrBasicStatus = [];
			for (let i in hive.oBases)
				Memory.arrBasicStatus[i] = false;
		}
		for (let ib in Memory.config.oBases)
			if (Memory.arrBasicStatus[ib] == null || Memory.arrBasicStatus[ib] == undefined)
				Memory.arrBasicStatus[ib] = false;

		// clean up dead creep memory...
		if (Game.time % 10 === 0)
		{
			for(var i in Memory.creeps) {
		        if(!Game.creeps[i]) {
		            delete Memory.creeps[i];
		        }
		    }
        }
		if (Memory.iBaseTresholdTimer == null || Memory.iBaseTresholdTimer == undefined) {
			Memory.iBaseTresholdTimer = 0;
		}
		if (Memory.iBaseTresholdTimer <= 0) {
			Memory.iBaseTresholdTimer = 50;
			m2_init.actualise_buildingThreshold(m2);
		} else {
			if (Game.time % 10 === 0) {
				Memory.iBaseTresholdTimer--;
			}
		}
		m2.usages.close(usid);
	},
    
	actualise_buildingThreshold : function (m2) {
		let rTargets = [];
		let iBaseThreshold = 25000;
		let iAvailableEnergy = 0;
		let iSmallestHit = -1;
		let iIncrement = 1000;
		
		for (let iF in Memory.config.oBases) {
			if (Game.rooms[Memory.config.oBases[iF].sRoom].storage) {
			iAvailableEnergy = 0;
				iAvailableEnergy = Game.rooms[Memory.config.oBases[iF].sRoom].storage.store[RESOURCE_ENERGY];
			}

			if (iAvailableEnergy < 100000) {
				Memory.config.oBases[iF].buildingHitThreshold = iBaseThreshold;
				Memory.config.oBases[iF].bBuilder = false;
			} else if (iAvailableEnergy > 350000) {
				rTargets = Game.rooms[Memory.config.oBases[iF].sRoom].find(
					FIND_STRUCTURES,
					{
						filter: (structure) =>
							structure.hits < structure.hitsMax &&
							structure.structureType === STRUCTURE_RAMPART
					}
				);
				if (rTargets.length > 0) {
					for (let iBuilding in rTargets) {
						if (iSmallestHit > 0) {
							if (rTargets[iBuilding].hits < iSmallestHit) {
								iSmallestHit = rTargets[iBuilding].hits;
							}
						} else {
							iSmallestHit = rTargets[iBuilding].hits
						}
					}
				}
				if (iSmallestHit < 0) {
					Memory.config.oBases[iF].bBuilder = false;
				} else {
					if (iSmallestHit < iBaseThreshold) {
						iSmallestHit = iBaseThreshold;
					}
					if (iSmallestHit < 3000000000) {
						Memory.config.oBases[iF].bBuilder = true;
						Memory.config.oBases[iF].buildingHitThreshold = iSmallestHit + iIncrement;
					} else {
						Memory.config.oBases[iF].bBuilder = false;
					}
				}
			}
		}
	},
	add_base : function (m2, sRoomName) {
		let oBaseToAdd = {
			sName: Math.random().toString(36).substr(2, 9),
			sRoom: sRoomName,
			energyCapacityAvailable: Game.rooms[sRoomName].energyCapacityAvailable,
			rSpawn: Array(),
			iLvl: Game.rooms[sRoomName].controller.level,
			arrEnergySources: Array(),
			bBuilder: false,
			bMiner : false,
			arrMineralSource: Array(),
			iMineralType : 0,
			sPowerSpawn: null,
			buildingHitThreshold: 10000,
			labs: Array(),
			lreact: Array()
		};

		let oSources = Game.rooms[sRoomName].find(FIND_SOURCES);
		if (oSources) {
			for (let iS in oSources) {
				oBaseToAdd.arrEnergySources.push(oSources[iS].id);
			}
		}
		let oMinerals = Game.rooms[sRoomName].find(FIND_MINERALS);
		if (oMinerals) {
			for (let iMI in oMinerals) {
				oBaseToAdd.arrMineralSource.push(oMinerals[iMI].id);
				oBaseToAdd.iMineralType = oMinerals[iMI].mineralType;
			}
		}

		let oBaseConfig = m2.tools.getStaticConfigByRoom(
			m2, sRoomName
		);
		if (oBaseConfig !== null) {
			oBaseToAdd.sName = oBaseConfig.sName;
			oBaseToAdd.labs = oBaseConfig.labs;
			oBaseToAdd.lreact = oBaseConfig.lreact;
		} else {
			console.log("[W] m2.init.run => (refresh) No static config found for room " + sRoomName);
		}

		Memory.config.oBases.push(oBaseToAdd);
	}
};

module.exports = m2_init;
