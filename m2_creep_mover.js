
var m2_creep_mover = {
	sName: 'module_creep_mover',
	sRole : 'mover',
	bSpawnLock : false,
	arrUnitsLimits : [0,0,0,0,1,1,1,1,1],
	arrBodies: [
		[null],	// lvl 0
		[null],	// lvl 1
		[null],	// lvl 2
		[null],	// lvl 3
		[
			MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY
		], // lvl 4
		[
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY
		], // lvl 5
		[
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY
		], // lvl 6
		[
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY
		], // lvl7
		[
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY
		], //lvl 8
		[
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE, MOVE,CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY,CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY,
			CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY
		] // lvl 9
	],
	arrUnitsByRoom : Array(),

	fnc : function (creep, prehook, m2) {
		if (!prehook)
				return ( true );
		let	oTarget = null;
		let rTagets = [];
		let oaction = null;
		let jobfound = false;
		let iMineral = 0;

		let iMBI = m2.tools.findRoomIndexInMemory(creep.room.name);
		if (iMBI < 0) {
			console.log("No base for room " + creep.room.name);
		}
		var oBase = Memory.config.oBases[iMBI];
		if (oBase == null) {

		}
		let iCapSize = 50;
		let iCareOf;
		if (oBase.iLvl > 7)
		{
			iCareOf = 800;
			iCapSize = 200;
		}
		else if (oBase.iLvl == 7)
		{
			iCapSize = 100;
			iCareOf = 800;
		}
		if (creep.memory.iMode == 0)
		{
			creep.memory.mo = 0;
			creep.memory.mp = null;
			creep.memory.sMissionName = "No mission..";
			creep.memory.targetId = null;
			creep.memory.nextMod = 0;
			creep.memory.iAmount = 0;
			creep.memory.iMineralType = RESOURCE_ENERGY;

			switch (creep.memory.iSeekStep) {
				case 0:
					rTagets = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
						filter: (structure) => {
							return (
								structure.structureType ===
								STRUCTURE_EXTENSION ||
								structure.structureType ===
								STRUCTURE_SPAWN
							) &&
							(structure.energy < structure.energyCapacity);
						}
					});

					if(rTagets) {
						creep.memory.sMissionName = "Fill Capacitors";
						creep.memory.targetId = rTagets.id;
						creep.memory.iMode = 1;
						creep.memory.nextMod = 5;
						jobfound = true;
					} else if (_.sum(creep.carry) > 0) {
						creep.memory.sMissionName = "Empty me";
						creep.memory.iMode = 10;
						creep.memory.nextMod = 0;
						jobfound = true;
					}

					break ;
				case 1:		// full containers - all but energy
					iMineral = 0;
					rTagets = Game.rooms[creep.room.name].find(
						FIND_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType
										== STRUCTURE_CONTAINER
									) &&
									structure.store.getFreeCapacity() == 0
							}
						}
					);
					iMineral = 0;
					if (rTagets.length > 0) {
						for (var iTa in rTagets) {
							//console.log(rTagets[iTa].store);
							if(iMineral == 0) {
								//console.log("now fetching mixed mineral");
								for (var imxnl in m2.config.arrMixedMinerals) {
									if (
										rTagets[iTa].store[
											m2.config.arrMixedMinerals[imxnl]
										] > 0
									){
										//console.log("mover. Mineral found." + iMineral);
										iMineral = m2.config.arrMixedMinerals[
											imxnl
										];
										break ;
									}
								}
							}
							if(iMineral == 0) {
								//console.log("now fetching factory mineral");
								for (var iFact in m2.config.arrFactoryMinerals) {
									if (rTagets[iTa].store[
											m2.config.arrFactoryMinerals[iFact]
										] > 0)
									{
										iMineral = m2.config.arrFactoryMinerals[
											iFact
										];
										break ;
									}
								}
							}
							if(iMineral == 0) {
								//console.log("now fetching regular mineral");
								for (var imnl in m2.config.arrMinerals)
								{
									if(
										m2.config.arrMinerals[imnl] !=
											RESOURCE_ENERGY &&
										rTagets[iTa].store[
											m2.config.arrMinerals[imnl]
										] > 0
									) {
										iMineral = m2.config.arrMinerals[imnl];
									}
									break ;
								}
							}
						}
						//console.log("There is a full container...");
					}
					//console.log("creep mover -> (full container) mineral ? (" + iMineral +
					//	") in room " + creep.room.name);
					if (iMineral != 0)
					{
						creep.memory.sMissionName = "Take ["+ iMineral +
							"] from full container " + rTagets[0].id;
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMineralType = iMineral;
						creep.memory.iMode = 11;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 2: 	// full containers - only energy
					iMineral = 0;
					rTagets = creep.room.find(
						FIND_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType
										== STRUCTURE_CONTAINER
									) &&
									structure.store.getFreeCapacity() == 0
							}
						}
					);
					if (rTagets.length > 0) {
						if(iMineral == 0) {
							if (
								rTagets[0].store[
									RESOURCE_ENERGY
								] > 0
							) {
								iMineral = RESOURCE_ENERGY;
								break ;
							}
						}
					}
					if (iMineral != 0)
					{
						creep.memory.sMissionName = "Take ["+ iMineral +
							"] from full container " + rTagets[0].id;
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMineralType = iMineral;
						creep.memory.iMode = 11;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 3:
					rTagets = creep.room.find(FIND_MY_STRUCTURES, {
						filter: (structure) => {
						    return (
						    	structure.structureType
						    	=== STRUCTURE_TOWER) &&
						    	(structure.energy <=
						    	(structure.energyCapacity / 2));
						}
					});
					if(rTagets.length > 0)
					{
						creep.memory.sMissionName = "Towers";
						creep.memory.iMode = 1;
						creep.memory.targetId = rTagets[0].id;
					 	creep.memory.nextMod = 5;
					 	jobfound = true;
					}
					break ;
				case 4:
					if (creep.room.terminal)
					{
						if (creep.room.terminal.store[RESOURCE_ENERGY] > 85000)
						{
							creep.memory.sMissionName =
								"remove energy from terminal";
							creep.memory.iMode = 12;
							creep.memory.nextMod = 10;
							creep.memory.iMineralType = RESOURCE_ENERGY;
							jobfound = true;
						}
					}
					break ;
				case 5:
					if (creep.room.terminal && creep.room.storage)
					{

						for (let iMinenalt in m2.config.arrMinerals) {

							if (m2.config.arrMinerals[
									iMinenalt
								] == RESOURCE_ENERGY
							) {
								if (creep.room.storage.store[
										RESOURCE_ENERGY
									] > 30000 &&
									creep.room.terminal.store[
										RESOURCE_ENERGY
									] < 80000
								) {
									creep.memory.sMissionName =
										"Put energy in terminal";
								 	creep.memory.iMode = 3;
								 	creep.memory.iMineralType =
								 		m2.config.arrMinerals[iMinenalt];
								 	creep.memory.nextMod = 4;
								 	jobfound = true;
								 	break ;
								}
							} else {
								if (
									creep.room.storage.store[
										m2.config.arrMinerals[iMinenalt]
									] >= iCareOf &&
									(
										creep.room.terminal.store[
											m2.config.arrMinerals[iMinenalt]
										] < 30000 ||
										creep.room.terminal.store[
											m2.config.arrMinerals[iMinenalt]
										] == null)
									) {
									creep.memory.sMissionName = "Put mineral ["
										+ m2.config.arrMinerals[iMinenalt] +
										"] in terminal";
									creep.memory.iMode = 3;
									creep.memory.iMineralType =
										m2.config.arrMinerals[iMinenalt];
								 	creep.memory.nextMod = 4;
								 	jobfound = true;
								 	break ;
								}
							}
						}
					}
					break ;
				case 6:
					if (creep.room.terminal && creep.room.storage) {
						for (let iMX in m2.config.arrMixedMinerals) {
							if (
								creep.room.storage.store[
									m2.config.arrMixedMinerals[iMX]
								] >= iCareOf &&
								(
									creep.room.terminal.store[
										m2.config.arrMixedMinerals[iMX]
									] < 30000
									|| creep.room.terminal.store[
										m2.config.arrMixedMinerals[iMX]
									] == null
								)
							) {
								creep.memory.sMissionName = "Put mineral [" +
									m2.config.arrMixedMinerals[iMX]
									+ "] in terminal";
								creep.memory.iMode = 3;
								creep.memory.iMineralType =
									m2.config.arrMixedMinerals[iMX];
							 	creep.memory.nextMod = 4;
							 	jobfound = true;
							}
						}
					}
					break ;
				case 7:
					if (creep.room.terminal) {
						for (var ilab in oBase.labs) {
							oTarget = Game.getObjectById(oBase.labs[ilab].uid);
							if (
								oTarget.mineralAmount <
									oTarget.mineralCapacity - iCareOf
								|| oTarget.mineralAmount == 0
							) {
								if (
									creep.room.storage.store[
										oBase.labs[ilab].r
									] >= iCareOf
								) {
									creep.memory.sMissionName =
										"Put mineral in lab from storage";
									creep.memory.targetId = oTarget.id;
									creep.memory.iMineralType =
										oBase.labs[ilab].r;
									creep.memory.iMode = 3;
									creep.memory.nextMod = 7;
									jobfound = true;
									break ;
								} else if (
									creep.room.terminal.store[
										oBase.labs[ilab].r
									] >= iCareOf
								) {
									creep.memory.sMissionName =
										"put mineral in lab from terminal";
									creep.memory.targetId = oTarget.id;
									creep.memory.iMineralType =
										oBase.labs[ilab].r;
									creep.memory.iMode = 12;
									creep.memory.nextMod = 7;
									jobfound = true;
									break ;
								}
							}
						}
					}
					break ;
				case 8:
					rTagets = creep.room.find(
						FIND_MY_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType ==
										STRUCTURE_LAB
									) &&
									structure.energy
									< structure.energyCapacity - 1000
							}
						}
					);
					if (rTagets.length > 0 ) {
						creep.memory.sMissionName = "Fill lab energy";
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMode = 1;
						creep.memory.nextMod = 5;
						jobfound = true;
					}
					break ;
				case 9:
					if (creep.room.terminal) {
						for (var iReact in oBase.lreact) {
							oTarget = Game.getObjectById(
								oBase.lreact[iReact].uidTo
							);
							if (oTarget) {
								if (
									oTarget.store[
										oBase.lreact[iReact].id
									] > 1500
								) {
									creep.memory.iAmount = 500;
									creep.memory.sMissionName =
										"Take mixed ressource to termial from "+
										"lab";
									creep.memory.targetId = oTarget.id;
									creep.memory.iMineralType =
										oBase.lreact[iReact].id;
									creep.memory.iMode = 11;
									creep.memory.nextMod = 10;
									jobfound = true;
									break ;
								}
							}

						}
					}

					// arrMixedMinerals
					break ;
				case 10:
					if (creep.room.terminal && creep.room.storage) {
						for (var iReact in oBase.lreact) {
							oTarget = Game.getObjectById(
								oBase.lreact[iReact].uidTo
							);
							if (oTarget) {
								if (
									oTarget.store[oBase.lreact[
										iReact].id
									] < 500
									|| oTarget.store[
										oBase.lreact[iReact].id
									] == null
								) {
									if (creep.room.terminal.store[
											oBase.lreact[iReact].id
										] >= 500
									) {
										// 8 take mineral from terminal
										creep.memory.iMode = 8;
										creep.memory.sMissionName =
											"Take mixed ressource to lab from "+
											"Terminal";

									} else if (creep.room.storage.store[
											oBase.lreact[iReact].id
										] >= 500
									) {
										// 6 Take mineral from storage
										creep.memory.iMode = 6;
										creep.memory.sMissionName =
											"Take mixed ressource to lab from "+
											"Storage";
									}
									if (creep.memory.iMode != 0) {
										creep.memory.iAmount = 500;
										creep.memory.targetId = oTarget.id;
										creep.memory.iMineralType =
											oBase.lreact[iReact].id;
										creep.memory.nextMod = 7;
										jobfound = true;
										break ;
									}
								}
							}
						}

					}

					break ;
				case 11:
					rTagets = creep.room.find(
						FIND_MY_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType ==
										STRUCTURE_LAB
									) &&
									structure.mineralType
									== RESOURCE_GHODIUM
									&&
									structure.mineralAmount >= iCareOf
							}
						}
					);
					if (rTagets.length > 0)
					{
						creep.memory.sMissionName = "Take ghodium from lab";
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMineralType = RESOURCE_GHODIUM;
						creep.memory.iMode = 11;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 12:
					rTagets = creep.room.find(
						FIND_MY_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType ==
										STRUCTURE_NUKER
									) &&
									structure.ghodium
									< structure.ghodiumCapacity
							}
						}
					);
					if (rTagets.length > 0)
					{
						if (
							creep.room.storage.store[RESOURCE_GHODIUM] > iCareOf
						)
						{
							creep.memory.sMissionName =
								"Put ghodium in nuke from Storage";
							creep.memory.targetId = rTagets[0].id;
							creep.memory.iMineralType = RESOURCE_GHODIUM;
							creep.memory.iAmount = 1000;
						    creep.memory.iMode = 9;
						    creep.memory.nextMod = 7;
						    jobfound = true;
						} else if (
							creep.room.terminal.store[
								RESOURCE_GHODIUM
							] > iCareOf)
						{
							creep.memory.sMissionName =
								"Put ghodium in nuke from Terminal";
							creep.memory.targetId = rTagets[0].id;
							creep.memory.iMineralType = RESOURCE_GHODIUM;
							creep.memory.iAmount = 1000;
						    creep.memory.iMode = 8;
						    creep.memory.nextMod = 7;
						    jobfound = true;
						}

					}
					break ;
				case 13:
					rTagets = creep.room.find(
						FIND_MY_STRUCTURES,
						{
							filter: (structure) => {
								return (
										structure.structureType ==
										STRUCTURE_NUKER
									) &&
									structure.energy
									< structure.energyCapacity
							}
						}
					);
					if (rTagets.length > 0)
					{
						creep.memory.sMissionName = "Put energy in nuke";
						creep.memory.iMode = 1;
						creep.memory.nextMod = 5;
						creep.memory.targetId = rTagets[0].id;
						jobfound = true;
					}
					break ;
				case 14:
					var oPowerSpawn = Game.getObjectById(oBase.sPowerSpawn);
					if (creep.room.terminal)
					{
						if (
							creep.room.terminal.store[RESOURCE_POWER] >= 100 &&
							oPowerSpawn
						) {
							if (oPowerSpawn.power == 0)
							{
								creep.memory.sMissionName =
									"Take power from terminal then to "+
									"powerspawn";
								creep.memory.iMode = 8;
								creep.memory.nextMod = 7;
								creep.memory.iAmount = 100;
								creep.memory.iMineralType = RESOURCE_POWER;
								creep.memory.targetId = oPowerSpawn.id;
								jobfound = true;
							}
						}
					}
					break ;
				case 15:
					var oPowerSpawn = Game.getObjectById(oBase.sPowerSpawn);
					if (creep.room.storage && oPowerSpawn)
					{
						if (creep.room.storage.store[RESOURCE_ENERGY] > iCareOf)
						{
							if (
								oPowerSpawn.energy <
									oPowerSpawn.energyCapacity - iCareOf
							){
								creep.memory.sMissionName =
									"Take energy to powerspawn";
								creep.memory.iMode = 1;
								creep.memory.nextMod = 5;
								creep.memory.iMineralType = RESOURCE_ENERGY;
								creep.memory.targetId = oPowerSpawn.id;
								jobfound = true;
							}
						}
					}
					break ;
				
				case 16:	// Put compressed mineral from factory to storage or
							// terminal
					if (
						creep.room.storage && Memory.config.oBases[
							iMBI
						].sFactoryId != "none"
					) {
						let oFactory = Game.getObjectById(
							Memory.config.oBases[iMBI].sFactoryId
						);
						if (oFactory) {
							let iFactoryRef = m2.tools.getFactoryBySrc(
								Memory.config.oBases[iMBI].iMineralType
							);
							if (oFactory.store[Memory.config.rRefineryBase[
									iFactoryRef].dest
								] >= iCareOf
							) {
								creep.memory.sMissionName =
									"Put factory stuff in storage";
								creep.memory.targetId = Memory.config.oBases[
									iMBI
								].sFactoryId;
								creep.memory.iMineralType =
									Memory.config.rRefineryBase[
										iFactoryRef
									].dest;
								creep.memory.iAmount = 0;
								creep.memory.iMode = 11;
								creep.memory.nextMod = 10;
								jobfound = true;
							}
						}
					}
					break;
				case 17:	// Put base mineral in rafinery to compress if too
							// much from terminal or storage
					if (
						creep.room.storage &&
						Memory.config.oBases[iMBI].sFactoryId != "none"
					) {
						let oFactory = Game.getObjectById(
							Memory.config.oBases[iMBI].sFactoryId
						);
						if (oFactory) {
							if (oFactory.store.getFreeCapacity() > 2000) {
								if (
									creep.room.storage.store[
										Memory.config.oBases[iMBI].iMineralType
									] > 16000 &&
									oFactory.store[Memory.config.oBases[
										iMBI].iMineralType
									] < 20000
								) {
									let iFactoryRef = m2.tools.getFactoryBySrc(
										Memory.config.oBases[iMBI].iMineralType
									);
									if (iFactoryRef != -1) {
										creep.memory.sMissionName =
											"Put Base mineral in factory from "+
											"Storage";
										creep.memory.targetId =
											Memory.config.oBases[
												iMBI
											].sFactoryId;
										creep.memory.iMineralType =
											Memory.config.oBases[
												iMBI
											].iMineralType;
										creep.memory.iAmount = 0;
										creep.memory.iMode = 9;
										creep.memory.nextMod = 7;
										jobfound = true;
									}
								}
							}
						}
					}
					break ;
				case 18:	// Put energy to factory
					if (
						creep.room.storage && Memory.config.oBases[
							iMBI
						].sFactoryId != "none"
					) {
						let oFactory = Game.getObjectById(
							Memory.config.oBases[iMBI].sFactoryId
						);
						if (oFactory) {
							if (oFactory.store.getFreeCapacity() > 2000) {
								if (oFactory.store[RESOURCE_ENERGY] < 10000) {
									let iFactoryRef =
										m2.tools.getFactoryBySrc(
											Memory.config.oBases[
												iMBI
											].iMineralType
										);
									if (iFactoryRef != -1) {
										creep.memory.sMissionName =
											"Put Energy in factory";
										creep.memory.targetId =
											Memory.config.oBases[
												iMBI
											].sFactoryId;
										creep.memory.iMineralType =
											RESOURCE_ENERGY;
										creep.memory.iAmount = 0;
										creep.memory.iMode = 1;
										creep.memory.nextMod = 5;
										jobfound = true;
									}
								}
							}
						}
					}
					break ;
				case 19:	// put refined mineral to terminal
					if (creep.room.storage && creep.room.terminal) {
						let iFactoryRef = m2.tools.getFactoryBySrc(
							Memory.config.oBases[iMBI].iMineralType
						);
						if (iFactoryRef != -1) {
							if (
								creep.room.storage.store[
									Memory.config.rRefineryBase[
										iFactoryRef
									].dest
								] >= iCareOf &&
								creep.room.terminal.store[
									Memory.config.rRefineryBase[
										iFactoryRef
									].dest
								] < 11000 &&
								creep.room.storage.store.getFreeCapacity() >=
									iCareOf
							) {
								creep.memory.sMissionName =
									"Put refined stuff from storage to " +
									"terminal";
								creep.memory.targetId = creep.room.terminal.id;
								creep.memory.iMineralType =
									Memory.config.rRefineryBase[
										iFactoryRef
									].dest;
								creep.memory.iAmount = 0;
								creep.memory.iMode = 3;
								creep.memory.nextMod = 4;
								jobfound = true;
							}
						}
						
						
					}
					break ;
				case 20:
					rTagets = creep.room.find(FIND_DROPPED_RESOURCES)
					if (rTagets.length > 0)
					{
						jobfound = true;
						creep.memory.targetId = rTagets[0].id;
						creep.memory.sMissionName =
							"Take stuff from dropped ressource";
						creep.memory.iMode = 13;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 21:
					iMineral = 0;
					for (var imnl in m2.config.arrMinerals) {
						rTagets = creep.room.find(
							FIND_STRUCTURES,
							{
								filter: (structure) => {
									return (
											structure.structureType
											== STRUCTURE_CONTAINER
										) &&
										structure.store[
											m2.config.arrMinerals[imnl]
										]>= iCareOf;
								}
							}
						);
						if (rTagets.length > 0)
						{
							iMineral = m2.config.arrMinerals[imnl];
							break ;
						}
					}
					if (iMineral != 0)
					{
						creep.memory.sMissionName =
							"Take ["+ iMineral +"] from container " +
							rTagets[0].id;
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMineralType = iMineral;
						creep.memory.iMode = 11;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 22:	// Empty containers
					iMineral = 0;
					for (var imnl in m2.config.arrMinerals) {
						rTagets = creep.room.find(
							FIND_STRUCTURES,
							{
								filter: (structure) => {
									return (
											structure.structureType
											== STRUCTURE_CONTAINER
										) &&
										structure.store[
											m2.config.arrMinerals[imnl]
										]> 0;
								}
							}
						);
						if (rTagets.length > 0)
						{
							iMineral = m2.config.arrMinerals[imnl];
							break ;
						}
					}
					if (iMineral != 0)
					{
						creep.memory.sMissionName =
							"Take ["+ iMineral +"] from container (empty) " +
							rTagets[0].id;
						creep.memory.targetId = rTagets[0].id;
						creep.memory.iMineralType = iMineral;
						creep.memory.iMode = 11;
						creep.memory.nextMod = 10;
						jobfound = true;
					}
					break ;
				case 23:
					if (creep.room.terminal)
					{
						if (
							_.sum(creep.room.terminal.store) + 1000 >
								creep.room.terminal.storeCapacity
						){
							creep.memory.sMissionName =
								"Take back base mineral from terminal to " +
								"storage";
							creep.memory.iMode = 12;
							creep.memory.nextMod = 10;
							creep.memory.iMineralType = oBase.iMineral;
							jobfound = true;
						}
					}
					break ;
				default:
					creep.memory.iSeekStep = 0;
					break ;
			}
			if (jobfound) {
				creep.memory.iSeekStep = 0;
				creep.say(".. Oh ^^'");
			} else {
				creep.say("ZZzz... " + creep.memory.iSeekStep);
				creep.memory.iSeekStep++;
			}
		}
		switch (creep.memory.iMode) {
			case 0:
				if (_.sum(creep.carry) > 0)
				{
					creep.memory.missionPath = null;
					creep.memory.iMode = 10;
				}
				break ;
			case 99 :
				m2.tools.findEnergy(creep);
				if (creep.carryCapacity == _.sum(creep.carry))
				{
					creep.memory.iMode = 1;
				}
				else
				break ;
			case 98 :

				break ;
			case 1: // collect energy from anywhere
				// clear up shit...
				if (
					creep.carry[creep.memory.iMineralType] !== _.sum(
						creep.carry
					)
				) {
					creep.memory.iMode = 10;
				}
				if (_.sum(creep.carry) < iCapSize)
					creep.memory.iMode = 99;
				else
				{
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.mo = 0;
					creep.memory.mp = null;
					creep.memory.nextMod = 0;
				}
				break ;
			case 2: // E Storage > Terminal
				if (creep.memory.iAmount === 0)
					oaction = creep.transfer(
						creep.room.terminal, RESOURCE_ENERGY
					);
				else
					oaction = creep.transfer(
						creep.room.terminal,
						RESOURCE_ENERGY,
						creep.memory.iAmount
					);
				if (oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.terminal);
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 3: // collect from <== Storage

				if (creep.memory.iAmount === 0)
					oaction = creep.withdraw(
						creep.room.storage,
						creep.memory.iMineralType
					);
				else
					oaction = creep.withdraw(
						creep.room.storage,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				if (oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.storage);
				} else if (oaction == ERR_FULL) {
					creep.memory.iMode = 10;
					creep.memory.nextMod = 0;
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 4: // Transfert to ==> Terminal
				if (creep.memory.iAmount === 0)
					oaction = creep.transfer(
						creep.room.terminal,
						creep.memory.iMineralType
					);
				else
					oaction = creep.transfer(
						creep.room.terminal,
						creep.memory.iMineralTypel,
						creep.memory.iAmount
					);
				if (oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.terminal);
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 5: // Move energy to target
				oTarget = Game.getObjectById(creep.memory.targetId);
				if (creep.memory.iAmount ===0)
					oaction = creep.transfer(oTarget, RESOURCE_ENERGY)
				else
					oaction = creep.transfer(
						oTarget,
						RESOURCE_ENERGY,
						creep.memory.iAmount
					);
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, oTarget);
	            } else if (oaction == ERR_NOT_ENOUGH_RESOURCES) {
					// Got to clean up.
					creep.memory.iMode = 10;
					creep.memory.nextMod = 0;
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 6: // Get Specific mineral from storage
				if (creep.memory.iAmount === 0)
					oaction = creep.withdraw(
						creep.room.storage,
						creep.memory.iMineralType
					);
				else
					oaction = creep.withdraw(
						creep.room.storage,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.storage);
	            } else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 7: // Move Specific mineral to target
					oTarget = Game.getObjectById(creep.memory.targetId);
					oaction = creep.transfer(
						oTarget, creep.memory.iMineralType
					);
					if(oaction == ERR_NOT_IN_RANGE) {
						m2.tools.mv(creep, oTarget);
					} else if(oaction == ERR_NOT_ENOUGH_RESOURCES) {
						creep.memory.iMode = 10;
						creep.memory.nextMod = 0;
		            } else {
						creep.memory.iMode = creep.memory.nextMod;
						creep.memory.nextMod = 0;
					}
				break ;
			case 8: // Get specific mineral from terminal
				if (creep.memory.iAmount == 0) {
					oaction = creep.withdraw(
						creep.room.terminal, creep.memory.iMineralType
					);
				} else {
					if (creep.memory.iAmount > creep.store.getCapacity()) {
						creep.memory.iAmount = creep.store.getCapacity();
					}
					oaction = creep.withdraw(
						creep.room.terminal,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				}
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.terminal);
	            } else if (oaction == OK) {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 9: // Get Specific mineral from storage
				if (creep.memory.iAmount === 0) {
					oaction = creep.withdraw(
						creep.room.storage, creep.memory.iMineralType
					);
				} else {
					oaction = creep.withdraw(
						creep.room.storage,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				}
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.storage);
	            } else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 11: // Get Specific mineral from target
				oTarget = Game.getObjectById(creep.memory.targetId);
				if (creep.memory.iAmount === 0)
					oaction = creep.withdraw(
						oTarget,
						creep.memory.iMineralType
					);
				else
					oaction = creep.withdraw(
						oTarget,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, oTarget);
	            } else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 12:
				if (creep.memory.iAmount === 0)
					oaction = creep.withdraw(
						creep.room.terminal,
						creep.memory.iMineralType
					);
				else
					oaction = creep.withdraw(
						creep.room.terminal,
						creep.memory.iMineralType,
						creep.memory.iAmount
					);
				if (oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, creep.room.terminal);
				} else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			case 13:
				oTarget = Game.getObjectById(creep.memory.targetId);
				oaction = creep.pickup(oTarget);
				if(oaction == ERR_NOT_IN_RANGE) {
					m2.tools.mv(creep, oTarget);
			    } else {
					creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
				}
				break ;
			default: // Move anything to storage
				iMineral = 0;
				for (var imnl in m2.config.arrMinerals) {
					if (creep.carry[m2.config.arrMinerals[imnl]])
					{
						iMineral = m2.config.arrMinerals[imnl];
						break ;
					}
				}
				if(iMineral == 0) {
					for (var imxnl in m2.config.arrMixedMinerals) {
						if (creep.carry[m2.config.arrMixedMinerals[imxnl]])
						{
							iMineral = m2.config.arrMixedMinerals[imxnl];
							break ;
						}
					}
				}
				if(iMineral == 0) {
					for (var iFact in m2.config.arrFactoryMinerals) {
						if (creep.carry[m2.config.arrFactoryMinerals[iFact]])
						{
							iMineral = m2.config.arrFactoryMinerals[iFact];
							break ;
						}
					}
				}
				if(iMineral != 0)
			 	{
					oaction = creep.transfer(creep.room.storage,
						iMineral
					);
					if(oaction == ERR_NOT_IN_RANGE) {
						m2.tools.mv(creep, creep.room.storage);
				    } else if (oaction == ERR_FULL) {
						for(const resourceType in creep.carry) {
							creep.drop(resourceType);
						}
					}
			 	}
			 	else
			 	{
			 		creep.memory.iMode = creep.memory.nextMod;
					creep.memory.nextMod = 0;
			 	}

				break;
		}

	},
	// Ajouté a hive.creephook
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_mover.sRole) {
			m2_creep_mover.arrUnitsByRoom.push(creep.memory.home);
			m2_creep_mover.fnc(creep, prehook, m2);
		}
	},
	// Lancé APRÈS hive
	basehook : function (m2, iBase) {
		let iCount = 0;
		let iLimit = m2_creep_mover.arrUnitsLimits[
			Memory.config.oBases[iBase].iLvl
		];
		for (let iB in m2_creep_mover.arrUnitsByRoom) {
			if (
				m2_creep_mover.arrUnitsByRoom[iB] ==
				Memory.config.oBases[iBase].sRoom
			) {
				iCount++;
			}
		}
		if (iLimit - iCount > 0 && !Memory.config.masterlock) {
			// should build
			for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
				let oSpawn = Game.spawns[
					Memory.config.oBases[iBase].rSpawn[iSpawn]
				];
				if (!oSpawn.spawning && !m2_creep_mover.bSpawnLock) {
					let arrBody = m2_creep_mover.arrBodies[
						Memory.config.oBases[iBase].iLvl
					];
					let iBuildReturn = oSpawn.spawnCreep(
						arrBody,
						'Joseph ' +
							iBase +
							Math.random().toString(36).substr(2, 3),
						{
							memory : {
								iMode: 0,
								hook: m2_creep_mover.sName,
								iSeekStep: 0,
								role: m2_creep_mover.sRole,
								home: Memory.config.oBases[iBase].sRoom
							}
						}
					);
					if (iBuildReturn == ERR_NOT_ENOUGH_ENERGY) {
						Memory.arrBasicStatus[iBase] = true;
						console.log(
							"m2_creep_mover; " +
							"Not enought energy => Enable basics for " +
							Memory.config.oBases[iBase].sRoom
						);
					} else if (iBuildReturn == OK) {
						Memory.arrBasicStatus[iBase] = false;
						console.log(
							"m2_creep_mover; Unit built in " +
							Memory.config.oBases[iBase].sRoom
						);
					} else {
						console.log(
							"m2_creep_mover; Unit not built in " +
							Memory.config.oBases[iBase].sRoom
						);
					}
					break ;
				}
			}
		}
	}
};

module.exports = m2_creep_mover;
