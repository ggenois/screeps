// m2_creep_builder

var m2_creep_builder = {
	sName: 'module_creep_builder',
	sRole : 'builder',
	bSpawnLock : false,
	arrUnitsLimits : [0,2,2,2,1,1,1,1,1],
	arrBodies: [
		[],
		[
			WORK, MOVE, CARRY,
		],
		[	// lvl 2 (550 Max)
			WORK, WORK, WORK,
			MOVE, MOVE, MOVE,
			CARRY, CARRY
		], // lvl 3 (800 Max)
		[	WORK, WORK, WORK, WORK, WORK,
			MOVE, MOVE, MOVE, MOVE,
			CARRY, CARRY
		],
		[	// lvl 4 (1300 Max)
	    		WORK, WORK, WORK, WORK, WORK, WORK,
	    		MOVE, MOVE, MOVE, MOVE, MOVE,
	    		CARRY, CARRY, CARRY
	  	],
	  	[	// lvl 5 (1800 Max)
	      	WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,
  			MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,
  			CARRY,CARRY,CARRY,CARRY,CARRY,CARRY
	  	],
	  	[	// lvl 6 (2300 max)
	  		WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,
  			MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,
  			CARRY,CARRY,CARRY,CARRY,CARRY,CARRY
	  	],
	  	[	// lvl 7 (5600 max)
	  	    WORK,WORK,WORK,WORK,WORK,
			WORK,WORK,WORK,WORK,WORK,
	  		//WORK,WORK,MOVE,
  			MOVE,MOVE,MOVE,MOVE,MOVE,
  			MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,
  			CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,
  			CARRY,CARRY,CARRY,CARRY,CARRY,CARRY
	  	],
	  	[	// lvl 8 (5600 max)
	  		WORK,WORK,WORK,WORK,WORK,
			WORK,WORK,WORK,WORK,WORK,
			MOVE,MOVE,MOVE,MOVE,MOVE,
			WORK,WORK,MOVE,MOVE,MOVE,
			CARRY,CARRY,CARRY,CARRY,CARRY,
  			MOVE,MOVE,MOVE,MOVE,MOVE,
			CARRY,CARRY,CARRY,CARRY,CARRY,
  			MOVE,MOVE,MOVE,MOVE,MOVE,
			CARRY,CARRY,CARRY,CARRY,CARRY,
  			MOVE,MOVE,MOVE,MOVE,MOVE

	  	]
	],
	arrUnitsByRoom : Array(),
	fnc : function (creep, prehook, m2) {
		if (!prehook)
			return ( true );
		let	oTarget;
		let rTargets;
		let oAction;
		let bJobFound;

		if (creep.memory.iSeek == undefined || creep.memory.iSeek == null) {
			creep.memory.iSeek = 0;
		}
		rTargets = [];
		oAction = null;
		bJobFound = false;
		
		if(creep.carry.energy === 0) {
			creep.memory.iMode = -1;
		}
		if (
			creep.memory.iMode == -1 && creep.carry.energy == creep.carryCapacity
		) {
			creep.memory.iMode = 0;
			creep.memory.iSeek = 0;
		}
		
		
		if (creep.memory.iMode == 0) {
			switch(creep.memory.iSeek) {
				case 0:
					rTargets = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
						filter: (structure) => {
							return (
								structure.structureType
								=== STRUCTURE_TOWER) &&
								(structure.energy <=
								(structure.energyCapacity / 2));
						}
					});
					if(rTargets) {
						creep.memory.iMode = 1;
						creep.memory.targetId = rTargets.id;
						bJobFound = true;
					}
					break ;
				case 2:
					rTargets = creep.room.find(
						FIND_STRUCTURES,
						{
							filter: structure => 
								structure.hits < structure.hitsMax &&
								(
									structure.structureType === STRUCTURE_RAMPART ||
									structure.structureType === STRUCTURE_WALL 
								)
						}
					);
					
					rTargets.sort((a,b) => a.hits - b.hits);
					
					if (rTargets.length > 0) {
						creep.memory.iMode = 3;
						creep.memory.targetId = rTargets[0].id;
						bJobFound = true;
					}
					break ;
				case 1:
					rTargets = creep.pos.findClosestByRange(
						FIND_CONSTRUCTION_SITES
					);
					if(rTargets) {
						creep.memory.iMode = 2;
						creep.memory.targetId = rTargets.id;
						bJobFound = true;
					}
					break ;
				case 3:
					rTargets = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
						filter: (structure) => {
							return (
								structure.structureType ===
								STRUCTURE_EXTENSION ||
								structure.structureType ===
								STRUCTURE_SPAWN
							) &&
							(structure.energy < structure.energyCapacity - creep.carryCapacity || structure.energy === 0);
						}
					});
					if (rTargets) {
						creep.memory.iMode = 4;
						creep.memory.targetId = rTargets.id;
						bJobFound = true;
					}
					break ;
				case 4:
					creep.memory.iMode = 5;
					bJobFound = true;
					break ;
				default:
					creep.memory.iSeek = -1;
					break ;
			}
			if (!bJobFound) {
				creep.memory.iSeek++;
			} else {
				creep.memory.iSeek = 0;
			}
		}
		switch (creep.memory.iMode) {
			case -1:	// Find energy
				m2.tools.findEnergy(creep);
				break ;
			case 1:	// fill towers
				oTarget = Game.getObjectById(creep.memory.targetId);
				oAction = creep.transfer(oTarget, RESOURCE_ENERGY);
				break ;
			case 2: // build construction site
				oTarget = Game.getObjectById(creep.memory.targetId);
				oAction = creep.build(oTarget);
				break ;
			case 3: // repair stuff
				oTarget = Game.getObjectById(creep.memory.targetId);
				if (oTarget) {
					if (oTarget.hits === oTarget.hitsMax)
						creep.memory.iMode = 0;
					oAction = creep.repair(oTarget);
				} else {
					creep.memory.iMode = 0;
				}
				break ;
			case 4: // fill capacitors
				oTarget = Game.getObjectById(creep.memory.targetId);
				oAction = creep.transfer(oTarget, RESOURCE_ENERGY);
				break ;
			default: // upgrade controler
				oTarget = creep.room.controller;
				oAction = creep.upgradeController(oTarget);
				break ;
		}
		if (creep.memory.iMode > 0) {
			if (oAction === ERR_NOT_IN_RANGE)
				creep.moveTo(oTarget);
			else if (oAction !== OK)
				creep.memory.iMode = 0;
		}

	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_builder.sRole) {
			m2_creep_builder.arrUnitsByRoom.push(creep.memory.home);
			m2_creep_builder.fnc(creep, prehook, m2);
		}
	},
	basehook : function (m2, iBase) {
		let iCount = 0;
		let iLimit = m2_creep_builder.arrUnitsLimits[Memory.config.oBases[iBase].iLvl];
		for (let iB in m2_creep_builder.arrUnitsByRoom) {
			if (
				m2_creep_builder.arrUnitsByRoom[iB] == Memory.config.oBases[iBase].sRoom
			) {
				iCount++;
			}
		}

		if (iLimit - iCount > 0 && Memory.config.oBases[iBase].bBuilder && !Memory.config.masterlock) {
			// should build
			for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
				let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
				if (!oSpawn.spawning && !m2_creep_builder.bSpawnLock) {
					let arrBody = m2_creep_builder.arrBodies[Memory.config.oBases[iBase].iLvl];
					let sBuildReturn = oSpawn.spawnCreep(
						arrBody,
						m2_creep_builder.sRole +
						'Arlido ' +
							iBase +
							Math.random().toString(36).substr(2, 3)
						,
						{
							memory : {
								hook: m2_creep_builder.sName,
								iMode: 0,
								role: m2_creep_builder.sRole,
								home: Memory.config.oBases[iBase].sRoom
							}
						}
					);
					console.log("m2_creep_builder; Unit built in " + Memory.config.oBases[iBase].sRoom + "[" + sBuildReturn +  "]");
					break ;
				}
			}
		}
	}
}

module.exports = m2_creep_builder;
