// m2_creep_mineral

var m2_creep_mineral = {
	sName: 'module_creep_mineral',
	sRole : 'mineral_harvester',
	bSpawnLock : false,
	arrUnitsLimits : [0,0,0,0,0,0,1,1,1],
	arrBodies: [
		[null],
		[null],
		[null],
		[null], // lvl 3
		[null], // lvl 4
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 5
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 6
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 7
		[
			WORK, WORK, WORK ,WORK ,WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE, MOVE, MOVE
		] // lvl 8
	],
	arrUnitsByRoom : Array(),
	fnc : function (creep, prehook, m2) {
		if (!prehook)
			return true;
		if (creep.memory.iMode === 0) {
			let obase = m2.tools.getBaseInMemoryByRoom(creep.room.name);
			creep.memory.source = obase.arrMineralSource[0];
			creep.memory.iMode = 1;
		} else if (creep.memory.iMode === 1) {
			let sources = Game.getObjectById(creep.memory.source);
			if (sources) {
				if (sources.mineralAmount > 0) {
					let iResponse = creep.harvest(sources);
					switch (iResponse) {
						case ERR_NOT_IN_RANGE:
							if (!m2.arrModules['module_build'].isInGrid(m2, creep)) {
								let rFound = creep.room.lookForAt(
									LOOK_STRUCTURES, creep.pos.x, creep.pos.y
								);
								if (rFound.length == 0) {
									rFound = creep.room.lookForAt(
										LOOK_CONSTRUCTION_SITES, creep.pos.x, creep.pos.y
									);
								}
								if (rFound.length == 0) {
									creep.room.createConstructionSite(
										creep.pos.x,
										creep.pos.y,
										STRUCTURE_ROAD
									);
								}
							}
							creep.moveTo(sources, {ignoreCreeps: true});
							break ;
						case OK:
							let rFound = creep.room.lookForAt(
								LOOK_STRUCTURES, creep.pos.x, creep.pos.y
							);
							if (rFound.length == 0) {
								rFound = creep.room.lookForAt(
									LOOK_CONSTRUCTION_SITES, creep.pos.x, creep.pos.y
								);
							}
							if (rFound.length == 0) {
								// Probably no container under it. Should build one
								creep.room.createConstructionSite(
									creep.pos.x,
									creep.pos.y,
									STRUCTURE_CONTAINER
								);
							}
							break ;
						default :
							break ;
					}
				}
			}
		}
	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_mineral.sRole) {
			let iBase = m2.tools.findRoomIndexInMemory(creep.memory.home);
			if (m2_creep_mineral.arrUnitsByRoom.indexOf(creep.memory.home) >= 0 || !Memory.config.oBases[iBase].bMiner) {
				creep.suicide();
			} else {
				m2_creep_mineral.arrUnitsByRoom.push(creep.memory.home);
			}
			m2_creep_mineral.fnc(creep, prehook, m2);
			delete(slot);
		}
	},
	basehook : function (m2, iBase) {
		let iCount = 0;
		let iLimit = m2_creep_mineral.arrUnitsLimits[Memory.config.oBases[iBase].iLvl];
		for (let iB in m2_creep_mineral.arrUnitsByRoom) {
			if (m2_creep_mineral.arrUnitsByRoom[iB] == Memory.config.oBases[iBase].sRoom) {
				iCount++;
			}
		}
		if (iLimit - iCount > 0 && Memory.config.oBases[iBase].bMiner && !Memory.config.masterlock) {
			// should build
			for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
				let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
				if (!oSpawn.spawning && !m2_creep_mineral.bSpawnLock) {
					let arrBody = m2_creep_mineral.arrBodies[Memory.config.oBases[iBase].iLvl];
					let iBuildRtn = oSpawn.spawnCreep(
						arrBody,

						'Chipi ' +
							iBase +
							Math.random().toString(36).substr(2, 3)
						,
						{
							memory : {
								iMode: 0,
								hook: m2_creep_mineral.sName,
								role: m2_creep_mineral.sRole,
								home: Memory.config.oBases[iBase].sRoom
							}
						}
					);
					if (iBuildRtn == OK) {
						console.log("m2_creep_mineral; Unit built in " + Memory.config.oBases[iBase].sRoom);
					} else if(iBuildRtn == ERR_NOT_ENOUGH_ENERGY) {
						console.log("m2_creep_mineral; No enough energy to build creep");
					}
					break ;
				}
			}
		}
	}
};

module.exports = m2_creep_mineral;
