// m2_config

var m2_config = {
	sUsername : 'c01',
	arrMinerals : [
		RESOURCE_ENERGY,
		RESOURCE_OXYGEN,
		RESOURCE_LEMERGIUM,
		RESOURCE_KEANIUM,
		RESOURCE_ZYNTHIUM,
		RESOURCE_UTRIUM,
		RESOURCE_GHODIUM,
		RESOURCE_UTRIUM_OXIDE,
		RESOURCE_POWER,
		RESOURCE_HYDROGEN,
		RESOURCE_CATALYST,
		RESOURCE_OPS
	],
	arrMixedMinerals : [
		RESOURCE_UTRIUM_HYDRIDE,
		RESOURCE_UTRIUM_OXIDE,
		RESOURCE_KEANIUM_HYDRIDE,
		RESOURCE_KEANIUM_OXIDE,
		RESOURCE_LEMERGIUM_HYDRIDE,
		RESOURCE_LEMERGIUM_OXIDE,
		RESOURCE_ZYNTHIUM_HYDRIDE,
		RESOURCE_ZYNTHIUM_OXIDE,
		RESOURCE_GHODIUM_HYDRIDE,
		RESOURCE_GHODIUM_OXIDE,
		RESOURCE_UTRIUM_LEMERGITE,
		RESOURCE_ZYNTHIUM_KEANITE,
		RESOURCE_HYDROXIDE
	],
	arrFactoryMinerals : [
		RESOURCE_UTRIUM_BAR,
		RESOURCE_LEMERGIUM_BAR,
		RESOURCE_ZYNTHIUM_BAR,
		RESOURCE_KEANIUM_BAR,
		RESOURCE_GHODIUM_MELT,
		RESOURCE_OXIDANT,
		RESOURCE_REDUCTANT,
		RESOURCE_PURIFIER,
		RESOURCE_BATTERY
	],
	/*

	*/
	arrEnergyByLevel : [
		0,
		300,
		550,
		800,
		1300,
		1800,
		2300,
		5600,
		12900
	],
	arrBaseBuildingModels : [
		{
			sName: 'init',
			iLvl: 0,
			arrWiredFrame : []
		},
		{
			sName: 'outpost',
			iLvl: 1,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN }
			]
		},
		{
			sName: 'ennampment',
			iLvl: 2,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION }
			]
		},
		{
			sName: 'Tower base',
			iLvl: 3,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER }
			]
		},
		{
			sName: 'Roady road storage',
			iLvl: 4,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER },
				{ x: 1, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 7, t: STRUCTURE_STORAGE },
				{ x: 0, y: 8, t: STRUCTURE_ROAD },
				{ x: 1, y: 4, t: STRUCTURE_ROAD },
				{ x: 1, y: 7, t: STRUCTURE_ROAD },
				{ x: 2, y: 5, t: STRUCTURE_ROAD },
				{ x: 2, y: 7, t: STRUCTURE_ROAD },
				{ x: 3, y: 6, t: STRUCTURE_ROAD },
				{ x: 3, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 6, t: STRUCTURE_ROAD },
				{ x: 5, y: 5, t: STRUCTURE_ROAD },
				{ x: 6, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 8, t: STRUCTURE_ROAD },
				{ x: 8, y: 5, t: STRUCTURE_ROAD },
				{ x: 8, y: 7, t: STRUCTURE_ROAD },
				{ x: 9, y: 4, t: STRUCTURE_ROAD },
				{ x: 9, y: 7, t: STRUCTURE_ROAD },
				{ x: 10, y: 8, t: STRUCTURE_ROAD }
			]
		},
		{
			sName: 'two tower',
			iLvl: 5,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER },
				{ x: 1, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 7, t: STRUCTURE_STORAGE },
				{ x: 0, y: 8, t: STRUCTURE_ROAD },
				{ x: 1, y: 4, t: STRUCTURE_ROAD },
				{ x: 1, y: 7, t: STRUCTURE_ROAD },
				{ x: 2, y: 5, t: STRUCTURE_ROAD },
				{ x: 2, y: 7, t: STRUCTURE_ROAD },
				{ x: 3, y: 6, t: STRUCTURE_ROAD },
				{ x: 3, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 6, t: STRUCTURE_ROAD },
				{ x: 5, y: 5, t: STRUCTURE_ROAD },
				{ x: 6, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 8, t: STRUCTURE_ROAD },
				{ x: 8, y: 5, t: STRUCTURE_ROAD },
				{ x: 8, y: 7, t: STRUCTURE_ROAD },
				{ x: 9, y: 4, t: STRUCTURE_ROAD },
				{ x: 9, y: 7, t: STRUCTURE_ROAD },
				{ x: 10, y: 8, t: STRUCTURE_ROAD },
				{ x: 0, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 7, t: STRUCTURE_TOWER }
			]
		},
		{
			sName: 'Terminal laby',
			iLvl: 6,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER },
				{ x: 1, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 7, t: STRUCTURE_STORAGE },
				{ x: 0, y: 8, t: STRUCTURE_ROAD },
				{ x: 1, y: 4, t: STRUCTURE_ROAD },
				{ x: 1, y: 7, t: STRUCTURE_ROAD },
				{ x: 2, y: 5, t: STRUCTURE_ROAD },
				{ x: 2, y: 7, t: STRUCTURE_ROAD },
				{ x: 3, y: 6, t: STRUCTURE_ROAD },
				{ x: 3, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 6, t: STRUCTURE_ROAD },
				{ x: 5, y: 5, t: STRUCTURE_ROAD },
				{ x: 6, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 8, t: STRUCTURE_ROAD },
				{ x: 8, y: 5, t: STRUCTURE_ROAD },
				{ x: 8, y: 7, t: STRUCTURE_ROAD },
				{ x: 9, y: 4, t: STRUCTURE_ROAD },
				{ x: 9, y: 7, t: STRUCTURE_ROAD },
				{ x: 10, y: 8, t: STRUCTURE_ROAD },
				{ x: 0, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 7, t: STRUCTURE_TOWER },
				{ x: 2, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 12, t: STRUCTURE_ROAD },
				{ x: 4, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 11, t: STRUCTURE_ROAD },
				{ x: 5, y: 9, t: STRUCTURE_ROAD },
				{ x: 5, y: 10, t: STRUCTURE_ROAD },
				{ x: 6, y: 8, t: STRUCTURE_ROAD },
				{ x: 6, y: 11, t: STRUCTURE_ROAD },
				{ x: 7, y: 12, t: STRUCTURE_ROAD },
				{ x: 5, y: 11, t: STRUCTURE_TERMINAL },
				{ x: 4, y: 5, t: STRUCTURE_LAB },
				{ x: 5, y: 4, t: STRUCTURE_LAB },
				{ x: 6, y: 5, t: STRUCTURE_LAB }
			]
		},
		{
			sName: 'City',
			iLvl: 7,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER },
				{ x: 1, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 7, t: STRUCTURE_STORAGE },
				{ x: 0, y: 8, t: STRUCTURE_ROAD },
				{ x: 1, y: 4, t: STRUCTURE_ROAD },
				{ x: 1, y: 7, t: STRUCTURE_ROAD },
				{ x: 2, y: 5, t: STRUCTURE_ROAD },
				{ x: 2, y: 7, t: STRUCTURE_ROAD },
				{ x: 3, y: 6, t: STRUCTURE_ROAD },
				{ x: 3, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 6, t: STRUCTURE_ROAD },
				{ x: 5, y: 5, t: STRUCTURE_ROAD },
				{ x: 6, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 8, t: STRUCTURE_ROAD },
				{ x: 8, y: 5, t: STRUCTURE_ROAD },
				{ x: 8, y: 7, t: STRUCTURE_ROAD },
				{ x: 9, y: 4, t: STRUCTURE_ROAD },
				{ x: 9, y: 7, t: STRUCTURE_ROAD },
				{ x: 10, y: 8, t: STRUCTURE_ROAD },
				{ x: 0, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 7, t: STRUCTURE_TOWER },
				{ x: 2, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 12, t: STRUCTURE_ROAD },
				{ x: 4, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 11, t: STRUCTURE_ROAD },
				{ x: 5, y: 9, t: STRUCTURE_ROAD },
				{ x: 5, y: 10, t: STRUCTURE_ROAD },
				{ x: 6, y: 8, t: STRUCTURE_ROAD },
				{ x: 6, y: 11, t: STRUCTURE_ROAD },
				{ x: 7, y: 12, t: STRUCTURE_ROAD },
				{ x: 5, y: 11, t: STRUCTURE_TERMINAL },
				{ x: 4, y: 5, t: STRUCTURE_LAB },
				{ x: 5, y: 4, t: STRUCTURE_LAB },
				{ x: 6, y: 5, t: STRUCTURE_LAB },
				{ x: 0, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 7, t: STRUCTURE_SPAWN },
				{ x: 3, y: 4, t: STRUCTURE_LAB },
				{ x: 4, y: 4, t: STRUCTURE_LAB },
				{ x: 7, y: 4, t: STRUCTURE_LAB },
				{ x: 7, y: 7, t: STRUCTURE_TOWER },
				{ x: 0, y: 9, t: STRUCTURE_ROAD },
				{ x: 0, y: 10, t: STRUCTURE_ROAD },
				{ x: 1, y: 11, t: STRUCTURE_ROAD },
				{ x: 2, y: 12, t: STRUCTURE_ROAD },
				{ x: 8, y: 12, t: STRUCTURE_ROAD },
				{ x: 9, y: 11, t: STRUCTURE_ROAD },
				{ x: 10, y: 10, t: STRUCTURE_ROAD },
				{ x: 10, y: 9, t: STRUCTURE_ROAD }
			]
		},
		{
			sName: 'Metropole',
			iLvl: 8,
			arrWiredFrame : [
				{ x: 5, y: 6, t: STRUCTURE_SPAWN },
				{ x: 0, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 5, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 6, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 5, t: STRUCTURE_TOWER },
				{ x: 1, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 8, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 7, t: STRUCTURE_STORAGE },
				{ x: 0, y: 8, t: STRUCTURE_ROAD },
				{ x: 1, y: 4, t: STRUCTURE_ROAD },
				{ x: 1, y: 7, t: STRUCTURE_ROAD },
				{ x: 2, y: 5, t: STRUCTURE_ROAD },
				{ x: 2, y: 7, t: STRUCTURE_ROAD },
				{ x: 3, y: 6, t: STRUCTURE_ROAD },
				{ x: 3, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 6, t: STRUCTURE_ROAD },
				{ x: 5, y: 5, t: STRUCTURE_ROAD },
				{ x: 6, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 6, t: STRUCTURE_ROAD },
				{ x: 7, y: 8, t: STRUCTURE_ROAD },
				{ x: 8, y: 5, t: STRUCTURE_ROAD },
				{ x: 8, y: 7, t: STRUCTURE_ROAD },
				{ x: 9, y: 4, t: STRUCTURE_ROAD },
				{ x: 9, y: 7, t: STRUCTURE_ROAD },
				{ x: 10, y: 8, t: STRUCTURE_ROAD },
				{ x: 0, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 9, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 3, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 4, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 7, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 7, t: STRUCTURE_TOWER },
				{ x: 2, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 6, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 12, t: STRUCTURE_ROAD },
				{ x: 4, y: 8, t: STRUCTURE_ROAD },
				{ x: 4, y: 11, t: STRUCTURE_ROAD },
				{ x: 5, y: 9, t: STRUCTURE_ROAD },
				{ x: 5, y: 10, t: STRUCTURE_ROAD },
				{ x: 6, y: 8, t: STRUCTURE_ROAD },
				{ x: 6, y: 11, t: STRUCTURE_ROAD },
				{ x: 7, y: 12, t: STRUCTURE_ROAD },
				{ x: 5, y: 11, t: STRUCTURE_TERMINAL },
				{ x: 4, y: 5, t: STRUCTURE_LAB },
				{ x: 5, y: 4, t: STRUCTURE_LAB },
				{ x: 6, y: 5, t: STRUCTURE_LAB },
				{ x: 0, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 1, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 10, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 11, t: STRUCTURE_EXTENSION },
				{ x: 10, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 4, y: 7, t: STRUCTURE_SPAWN },
				{ x: 3, y: 4, t: STRUCTURE_LAB },
				{ x: 4, y: 4, t: STRUCTURE_LAB },
				{ x: 7, y: 4, t: STRUCTURE_LAB },
				{ x: 7, y: 7, t: STRUCTURE_TOWER },
				{ x: 0, y: 9, t: STRUCTURE_ROAD },
				{ x: 0, y: 10, t: STRUCTURE_ROAD },
				{ x: 1, y: 11, t: STRUCTURE_ROAD },
				{ x: 2, y: 12, t: STRUCTURE_ROAD },
				{ x: 8, y: 12, t: STRUCTURE_ROAD },
				{ x: 9, y: 11, t: STRUCTURE_ROAD },
				{ x: 10, y: 10, t: STRUCTURE_ROAD },
				{ x: 10, y: 9, t: STRUCTURE_ROAD },
				{ x: 6, y: 7, t: STRUCTURE_SPAWN },
				{ x: 5, y: 8, t: STRUCTURE_POWER_SPAWN },
				{ x: 3, y: 3, t: STRUCTURE_TOWER },
				{ x: 7, y: 3, t: STRUCTURE_TOWER },
				{ x: 7, y: 5, t: STRUCTURE_TOWER },
				{ x: 4, y: 1, t: STRUCTURE_OBSERVER },
				{ x: 6, y: 1, t: STRUCTURE_FACTORY },
				{ x: 1, y: 1, t: STRUCTURE_LINK },
				{ x: 9, y: 1, t: STRUCTURE_LINK },
				{ x: 5, y: 2, t: STRUCTURE_NUKER },
				{ x: 4, y: 3, t: STRUCTURE_LAB },
				{ x: 5, y: 3, t: STRUCTURE_LAB },
				{ x: 6, y: 3, t: STRUCTURE_LAB },
				{ x: 6, y: 4, t: STRUCTURE_LAB },
				{ x: 1, y: 2, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 1, t: STRUCTURE_EXTENSION },
				{ x: 2, y: 2, t: STRUCTURE_EXTENSION },
				{ x: 3, y: 1, t: STRUCTURE_EXTENSION },
				{ x: 7, y: 1, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 1, t: STRUCTURE_EXTENSION },
				{ x: 8, y: 2, t: STRUCTURE_EXTENSION },
				{ x: 9, y: 2, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 0, t: STRUCTURE_EXTENSION },
				{ x: 5, y: 12, t: STRUCTURE_EXTENSION },
				{ x: 0, y: 1, t: STRUCTURE_ROAD },
				{ x: 0, y: 2, t: STRUCTURE_ROAD },
				{ x: 0, y: 3, t: STRUCTURE_ROAD },
				{ x: 1, y: 0, t: STRUCTURE_ROAD },
				{ x: 2, y: 0, t: STRUCTURE_ROAD },
				{ x: 2, y: 3, t: STRUCTURE_ROAD },
				{ x: 3, y: 0, t: STRUCTURE_ROAD },
				{ x: 3, y: 2, t: STRUCTURE_ROAD },
				{ x: 4, y: 0, t: STRUCTURE_ROAD },
				{ x: 4, y: 2, t: STRUCTURE_ROAD },
				{ x: 5, y: 1, t: STRUCTURE_ROAD },
				{ x: 6, y: 0, t: STRUCTURE_ROAD },
				{ x: 6, y: 2, t: STRUCTURE_ROAD },
				{ x: 7, y: 0, t: STRUCTURE_ROAD },
				{ x: 7, y: 2, t: STRUCTURE_ROAD },
				{ x: 8, y: 0, t: STRUCTURE_ROAD },
				{ x: 8, y: 3, t: STRUCTURE_ROAD },
				{ x: 9, y: 0, t: STRUCTURE_ROAD },
				{ x: 10, y: 1, t: STRUCTURE_ROAD },
				{ x: 10, y: 2, t: STRUCTURE_ROAD },
				{ x: 10, y: 3, t: STRUCTURE_ROAD }
			]
		}
	],
	oBases : [
		{
			sName: 'Homebase',
			sRoom: 'E21N19',
			labs: [
			{
				uid: '5c4ba8233697710695a45f07',
				r: RESOURCE_ZYNTHIUM
			},
			{
				uid: '5c4be5941f45322920382fff',
				r: RESOURCE_KEANIUM
			},
			{
				uid: '5c886092296173525ecf19ab',
				r: RESOURCE_UTRIUM
			},
			{
				uid: '5c87f807e8d9eb1e544d20dc',
				r: RESOURCE_LEMERGIUM
			},
			{
				uid: '5c891167487dbc155038d467',
				r: RESOURCE_OXYGEN
			}
			],
			lreact : [

			{
				sName : 'zynthium keanite',
				id : RESOURCE_ZYNTHIUM_KEANITE,
				uidTo : '5c5a77093df79122d10e5906',
				uidSrcA : '5c4ba8233697710695a45f07',
				uidSrcB : '5c4be5941f45322920382fff'
			},
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5c88cd89e8d9eb1e544d725f',
				uidSrcA : '5c886092296173525ecf19ab',
				uidSrcB : '5c891167487dbc155038d467'
			},
			{
				sName : 'utrium lemergite',
				id : RESOURCE_UTRIUM_LEMERGITE,
				uidTo : '5c5ac8036f956a230ba0924c',
				uidSrcA : '5c886092296173525ecf19ab',
				uidSrcB : '5c87f807e8d9eb1e544d20dc'
			},
			{
				sName : 'ghodium',
				id : RESOURCE_GHODIUM,
				uidTo : '5c4c223b4e71c8290e092800',
				uidSrcA : '5c5a77093df79122d10e5906',
				uidSrcB : '5c5ac8036f956a230ba0924c'
			}
			/*{
				sName : 'ghodium',
				id : RESOURCE_GHODIUM,
				uidTo : '5c5ac8036f956a230ba0924c',
				uidSrcA : '5c5a77093df79122d10e5906',
				uidSrcB : '5c5b1501c15e0012c3fe673b'
			},
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5c886092296173525ecf19ab',
				uidSrcA : '5c87f807e8d9eb1e544d20dc',
				uidSrcB : '5c891167487dbc155038d467'
			}
			*/
			]
		},
		{
			sName: 'Alpha Century',
			sRoom: 'E21N18',
			labs: [
			{
				uid: '5c56a096ded7de22c7e0a5a8',
				r : RESOURCE_UTRIUM
			},
			{
				uid: '5c69876cded7de22c7e76bcf',
				r : RESOURCE_OXYGEN
			},
			{
				uid: '5c69ebcbe06a1e67de3da7a0',
				r : RESOURCE_LEMERGIUM
			},
			{
				uid: '5cb5fd2790898927775eb0c0',
				r : RESOURCE_ZYNTHIUM
			},
			{
				uid: '5cb5cffcca69d7204f0dc364',
				r : RESOURCE_KEANIUM
			}
			],
			lreact : [
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5c55fb53d1affa175929d322',
				uidSrcA : '5c56a096ded7de22c7e0a5a8',
				uidSrcB : '5c69876cded7de22c7e76bcf'
			},
			{
				sName : 'utrium LEMERGITE',
				id : RESOURCE_UTRIUM_LEMERGITE,
				uidTo : '5c5bf590d988df67ed4491f7',
				uidSrcA : '5c56a096ded7de22c7e0a5a8',
				uidSrcB : '5c69ebcbe06a1e67de3da7a0'
			},
			{
				sName : 'ZYNTHIUM_KEANITE',
				id : RESOURCE_ZYNTHIUM_KEANITE,
				uidTo : '5c6a0d8aff1cf36509f1cbfc',
				uidSrcA : '5cb5fd2790898927775eb0c0',
				uidSrcB : '5cb5cffcca69d7204f0dc364'
			},
			{
				sName : 'ghodium',
				id : RESOURCE_GHODIUM,
				uidTo : '5cb55acd77319c11499be104',
				uidSrcA : '5c6a0d8aff1cf36509f1cbfc',
				uidSrcB : '5c5bf590d988df67ed4491f7'
			}
			]
		},
		{
			sName: 'Constance',
			sRoom: 'E22N16',
			labs: [
			{
				uid: '5c6aac47b819591786d34180',
				r : RESOURCE_ZYNTHIUM
			},
			{
				uid: '5c584c347bd8e01765e17d8a',
				r : RESOURCE_KEANIUM
			},
			{
				uid: '5c6b575aa259436814c3b6e2',
				r : RESOURCE_UTRIUM
			},
			{
				uid: '5ca72147c273b656077b7c19',
				r : RESOURCE_LEMERGIUM
			},
			{
				uid: '5ca7a7179da478524861e3e7',
				r : RESOURCE_OXYGEN
			}
			],
			lreact : [
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5ca6e2088609a444bad604af',
				uidSrcA : '5c6b575aa259436814c3b6e2',
				uidSrcB : '5ca7a7179da478524861e3e7'
			},
			{
				sName : 'utrium LEMERGITE',
				id : RESOURCE_UTRIUM_LEMERGITE,
				uidTo : '5c6af80daa0b8422e650e3a9',
				uidSrcA : '5c6b575aa259436814c3b6e2',
				uidSrcB : '5ca72147c273b656077b7c19'
			},
			{
				sName : 'ZYNTHIUM_KEANITE',
				id : RESOURCE_ZYNTHIUM_KEANITE,
				uidTo : '5c58203baa7ed5651d1de2f3',
				uidSrcA : '5c6aac47b819591786d34180',
				uidSrcB : '5c584c347bd8e01765e17d8a'
			},
			{
				sName : 'ghodium',
				id : RESOURCE_GHODIUM,
				uidTo : '5c579d3d19fea10edd7d8f8a',
				uidSrcA : '5c6af80daa0b8422e650e3a9',
				uidSrcB : '5c58203baa7ed5651d1de2f3'
			}
			]
		},
		{
			sName: 'Esperance',
			sRoom: 'E22N18',
			labs: [{
				uid: '5c6751efa9dd9767ef66cd76',
				r: RESOURCE_ZYNTHIUM
			},
			{
				uid: '5cc89e31e8697a50877c9b7b',
				r: RESOURCE_KEANIUM
			},
			{
				uid: '5c66e987b274ce22c56390fa',
				r: RESOURCE_UTRIUM
			},
			{
				uid: '5c89c962eaf0641346eea04c',
				r: RESOURCE_LEMERGIUM
			},
			{
				uid: '5cc906be47dcb56099e79339',
				r : RESOURCE_OXYGEN
			}],
			lreact : [
				{
					sName : 'utrium oxide',
					id : RESOURCE_UTRIUM_OXIDE,
					uidTo : '5cc9e605b5e4b70b9296392c',
					uidSrcA : '5c66e987b274ce22c56390fa',
					uidSrcB : '5cc906be47dcb56099e79339'
				},
				{
					sName : 'utrium LEMERGITE',
					id : RESOURCE_UTRIUM_LEMERGITE,
					uidTo : '5c8979febe3bd3525dc5a64c',
					uidSrcA : '5c66e987b274ce22c56390fa',
					uidSrcB : '5c89c962eaf0641346eea04c'
				},
				{
					sName : 'ZYNTHIUM_KEANITE',
					id : RESOURCE_ZYNTHIUM_KEANITE,
					uidTo : '5c8945ea23d6701313d19583',
					uidSrcA : '5c6751efa9dd9767ef66cd76',
					uidSrcB : '5cc89e31e8697a50877c9b7b'
				},
				{
					sName : 'ghodium',
					id : RESOURCE_GHODIUM,
					uidTo : '5c67d239ded7de22c7e6cfe4',
					uidSrcA : '5c8979febe3bd3525dc5a64c',
					uidSrcB : '5c8945ea23d6701313d19583'
				}
			]
		},
		{
			sName: 'Romy',
			sRoom: 'E24N17',
			labs: [
			{
				uid: '5cdc85df4675443e98b56027',
				r: RESOURCE_ZYNTHIUM
			},
			{
				uid: '5c72ff49c220f5616ab751ea',
				r: RESOURCE_KEANIUM
			},
			{
				uid: '5c9613addc38b4513e1aeb0f',
				r: RESOURCE_UTRIUM
			},
			{
				uid: '5cdc9a56ca3de54ed7ee273c',
				r: RESOURCE_LEMERGIUM
			},
			{
				uid: '5c95ee0385f4d772f2968577',
				r : RESOURCE_OXYGEN
			}
			],
			lreact : [
				{
					sName : 'utrium oxide',
					id : RESOURCE_UTRIUM_OXIDE,
					uidTo : '5cdccd90d947f15aa496b322',
					uidSrcA : '5c9613addc38b4513e1aeb0f',
					uidSrcB : '5c95ee0385f4d772f2968577'
				},
				{
					sName : 'utrium LEMERGITE',
					id : RESOURCE_UTRIUM_LEMERGITE,
					uidTo : '5c9553e35ce735539f9642ae',
					uidSrcA : '5c9613addc38b4513e1aeb0f',
					uidSrcB : '5cdc9a56ca3de54ed7ee273c'
				},
				{
					sName : 'ZYNTHIUM_KEANITE',
					id : RESOURCE_ZYNTHIUM_KEANITE,
					uidTo : '5c72c3bbb7d2a441c40453ea',
					uidSrcA : '5cdc85df4675443e98b56027',
					uidSrcB : '5c72ff49c220f5616ab751ea'
				},
				{
					sName : 'ghodium',
					id : RESOURCE_GHODIUM,
					uidTo : '5c734fb40f30346160f22143',
					uidSrcA : '5c9553e35ce735539f9642ae',
					uidSrcB : '5c72c3bbb7d2a441c40453ea'
				}
			]
		},
		{
			sName: 'Sophie',
			sRoom: 'E23N18',
			labs: [],
			lreact : []
		},
		{
			sName: 'Hope',
			sRoom: 'E22N19',
			labs: [
			{
				uid: '5d935c9902d8eb0001120eba',
				r : RESOURCE_UTRIUM
			},
			{
				uid: '5cc388258fb11107ab12d467',
				r : RESOURCE_OXYGEN
			}
			],
			lreact : [
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5cc365a4a67fbd10062b9dbc',
				uidSrcA : '5d935c9902d8eb0001120eba',
				uidSrcB : '5cc388258fb11107ab12d467'
			}],
		},
		{
			sName: 'Cindy',
			sRoom: 'E19N19',
			labs: [{
				uid: '5cde27566664dc372fa8cd5a',
				r : RESOURCE_KEANIUM
			},
			{
				uid: '5d5bf35155e4f473c881e03f',
				r : RESOURCE_HYDROGEN
			},
			{
				uid: '5cdd9c73498f384ef0d608a3',
				r : RESOURCE_ZYNTHIUM
			},
			{
				uid: '5d065795ab6f8a31fa8a88d2',
				r : RESOURCE_UTRIUM
			},
			{
				uid: '5d5b81e60ebed21e32712615',
				r : RESOURCE_LEMERGIUM
			},
			{
				uid: '5cdd9c73498f384ef0d608a3',
				r : RESOURCE_OXYGEN
			}
			],
			lreact : [
			{
				sName : 'utrium oxide',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5d0620373edbdf532d3dd0b5',
				uidSrcA : '5d065795ab6f8a31fa8a88d2',
				uidSrcB : '5cdd9c73498f384ef0d608a3'
			},
			/*{
				sName : 'utrium LEMERGITE',
				id : RESOURCE_UTRIUM_LEMERGITE,
				uidTo : '5d063f16310b6210fc300bab',
				uidSrcA : '5d065795ab6f8a31fa8a88d2',
				uidSrcB : '5d5b81e60ebed21e32712615'
			},*/
			{
				sName : 'ZYNTHIUM_KEANITE',
				id : RESOURCE_ZYNTHIUM_KEANITE,
				uidTo : '5cddd3e3471cad60c618f6c8',
				uidSrcA : '5cdd9c73498f384ef0d608a3',
				uidSrcB : '5cde27566664dc372fa8cd5a'
			},
			/*{
				sName : 'ghodium',
				id : RESOURCE_GHODIUM,
				uidTo : '5d0620373edbdf532d3dd0b5',
				uidSrcA : '5d063f16310b6210fc300bab',
				uidSrcB : '5cddd3e3471cad60c618f6c8'
			},*/
			{
				sName : 'keanium hydride',
				id : RESOURCE_KEANIUM_HYDRIDE,
				uidTo : '5d5c1813b8211979f8dbc6fe',
				uidSrcA : '5cde27566664dc372fa8cd5a',
				uidSrcB : '5d5bf35155e4f473c881e03f'
			}]
		},
		{
			sName: 'Veronik',
			sRoom: 'E23N17',
			labs: [{
				uid: '5d18084c3df4dc3871310f98',
				r : RESOURCE_KEANIUM
			},
			{
				uid: '5cfe1b2dc423a831c532d9df',
				r : RESOURCE_HYDROGEN
			}],
			lreact : []
		},
		{
			sName: 'Rouh',
			sRoom: 'E22N21',
			labs: [{
				uid: '5d939c23744a7b0001a4f4d9',
				r : RESOURCE_OXYGEN
			},
			{
				uid: '5dbac7fe2c520ddd43fedf89',
				r : RESOURCE_KEANIUM
			},
			{
				uid: '5d9381aada9a000001cf9fe6',
				r : RESOURCE_UTRIUM
			},
			{
				uid: '5d93693e0cdbd4000194839e',
				r : RESOURCE_HYDROGEN
			},
			{
				uid: '5d48e137d2b64f7a1b132c29',
				r : RESOURCE_LEMERGIUM
			}],
			lreact : [{
				sName : 'RESOURCE_UTRIUM_OXIDE',
				id : RESOURCE_UTRIUM_OXIDE,
				uidTo : '5dbb0e19afbb0353a85c0748',
				uidSrcA : '5d9381aada9a000001cf9fe6',
				uidSrcB : '5d939c23744a7b0001a4f4d9'
			},
			{
				sName : 'RESOURCE_KEANIUM_OXIDE',
				id : RESOURCE_KEANIUM_HYDRIDE,
				uidTo : '5dbadafc3fc04ae76736c665',
				uidSrcA : '5dbac7fe2c520ddd43fedf89',
				uidSrcB : '5d93693e0cdbd4000194839e'
			},
			{
				sName : 'RESOURCE_KEANIUM_OXIDE',
				id : RESOURCE_KEANIUM_OXIDE,
				uidTo : '5dbaf47fd67a548d7b9f74e4',
				uidSrcA : '5dbac7fe2c520ddd43fedf89',
				uidSrcB : '5d939c23744a7b0001a4f4d9'
			},
			{
				sName : 'RESOURCE_LEMERGIUM_HYDRIDE',
				id : RESOURCE_LEMERGIUM_HYDRIDE,
				uidTo : '5d491c1f14989673a6263b34',
				uidSrcA : '5d48e137d2b64f7a1b132c29',
				uidSrcB : '5d93693e0cdbd4000194839e'
			},
			{
				sName : 'RESOURCE_UTRIUM_LEMERGITE',
				id : RESOURCE_UTRIUM_LEMERGITE,
				uidTo : '5d48ae5612fdad20d9ce476e',
				uidSrcA : '5d9381aada9a000001cf9fe6',
				uidSrcB : '5d48e137d2b64f7a1b132c29'
			}]
		},
		{
			sName: 'minirouh',
			sRoom: 'E23N22',
			labs: [],
			lreact : []
		},
		{
			sName: 'minirouhette',
			sRoom: 'E26N19',
			labs: [],
			lreact : []
		}
	],
	getConfigForRoom : function (sRoom) {
		for (let iIndex in m2_config.oBases) {
			if (m2_config.oBases[iIndex].sRoom == sRoom) {
				return (m2_config.oBases[iIndex]);
			}
		}
		return (null);
	}
}

module.exports = m2_config;
