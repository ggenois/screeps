// m2_market

var m2_market = {
	sName : 'm2_market',
	trade : function (m2) {
		let iAmount;
		let iSurplus;
		let iTolaratedCost;
		let arrPriceTab;
		let	iDeals;
		
		if (Memory.config.marketLock == undefined || Memory.config.marketLock == null) {
			Memory.config.marketLock = false;
		}
		if (!Memory.config.marketLock && Game.time % 10 === 0)
		{
			iDeals = 10;
			iSurplus = 22500;
			iAmount = 5000;
			iTolaratedCost = 9000;
			arrPriceTab = [
				{s: "RESOURCE_OXYGEN",			r : RESOURCE_OXYGEN,		m : 1.140},
				{s: "RESOURCE_KEANIUM",			r : RESOURCE_KEANIUM,		m : 1.140},
				{s: "RESOURCE_HYDROGEN",		r : RESOURCE_HYDROGEN,		m : 1.140},
				{s: "RESOURCE_ZYNTHIUM",		r : RESOURCE_ZYNTHIUM,		m : 1.140},
				{s: "RESOURCE_LEMERGIUM",		r : RESOURCE_LEMERGIUM,		m : 1.140},
				{s: "RESOURCE_UTRIUM",			r : RESOURCE_UTRIUM,		m : 1.140},
				{s: "RESOURCE_UTRIUM_BAR",		r : RESOURCE_UTRIUM_BAR,	m : 0.140},
				{s: "RESOURCE_LEMERGIUM_BAR",	r : RESOURCE_LEMERGIUM_BAR,	m : 0.140},
				{s: "RESOURCE_ZYNTHIUM_BAR",	r : RESOURCE_ZYNTHIUM_BAR,	m : 0.140},
				{s: "RESOURCE_KEANIUM_BAR",		r : RESOURCE_KEANIUM_BAR,	m : 0.140},
				{s: "RESOURCE_GHODIUM_MELT",	r : RESOURCE_GHODIUM_MELT,	m : 0.140},
				{s: "RESOURCE_REDUCTANT",		r : RESOURCE_REDUCTANT,		m : 0.140},
				{s: "RESOURCE_PURIFIER",		r : RESOURCE_PURIFIER,		m : 0.140},
				{s: "RESOURCE_BATTERY",			r : RESOURCE_BATTERY,		m : 0.140}
			];

			for (var ibase in Memory.config.oBases)
			{
				let mini = 0;
				let sTabName = "[Error]";
				
				
					
				/* Disabled; Do not sell unrefined ressources. Refine them fisrt.
				 *
				if (Game.rooms[Memory.config.oBases[ibase].sRoom].terminal) {
				for (var iP in arrPriceTab) {
					if (
						arrPriceTab[iP].r == Memory.config.oBases[ibase].iMineralType
					) {
						sTabName = arrPriceTab[iP].s;
						mini = arrPriceTab[iP].m;
					}
				}
				
					if (
						Game.rooms[
							Memory.config.oBases[ibase].sRoom
						].terminal.cooldown == 0 &&
						Game.rooms[
							Memory.config.oBases[ibase].sRoom
						].terminal.store[
							Memory.config.oBases[ibase].iMineralType
						] > iAmount + iSurplus &&
						mini > 0
					) {
						let oOrders = Game.market.getAllOrders({
							type: ORDER_BUY,
							resourceType: Memory.config.oBases[ibase].iMineral
						});

						for (var iOrder in oOrders) {
							if (
								oOrders[iOrder].price > mini && oOrders[iOrder].remainingAmount >= iAmount
							) {
								var iPrice = Game.market.calcTransactionCost(
									iAmount,
									Memory.config.oBases[ibase].sRoom, oOrders[iOrder].roomName
								);
								if (iPrice < iTolaratedCost && iDeals > 0) {
									iDeals--;
									var rtn = Game.market.deal(
										oOrders[iOrder].id,
										iAmount,
										Memory.config.oBases[ibase].sRoom
									);
									console.log("Mineral deal.[" + oOrders[iOrder].price + "]-" + rtn);
								}
							}
						}
					}
				}
					*/
				
				if (Game.rooms[Memory.config.oBases[ibase].sRoom].terminal) {
				
					iSurplus = 5000;
					iAmount = 1000;
					if (
						Game.rooms[
							Memory.config.oBases[ibase].sRoom
						].terminal.cooldown == 0
					) {
						let iFactRef = m2.tools.getFactoryBySrc(Memory.config.oBases[ibase].iMineralType);
						if (iFactRef >= 0) {
							for (var iP in arrPriceTab) {
								if (
									arrPriceTab[iP].r == Memory.config.rRefineryBase[iFactRef].dest
								) {
									sTabName = arrPriceTab[iP].s;
									mini = arrPriceTab[iP].m;
								}
							}
							
							if (
								Game.rooms[
									Memory.config.oBases[ibase].sRoom
								].terminal.store[
									Memory.config.rRefineryBase[iFactRef].dest
								] > iAmount + iSurplus &&
								mini > 0
							) {
								let oOrders = Game.market.getAllOrders({
									type: ORDER_BUY,
									resourceType: Memory.config.rRefineryBase[iFactRef].dest
								});
								for (var iOrder in oOrders) {
									
									if (
										oOrders[iOrder].price > mini && oOrders[iOrder].remainingAmount >= iAmount
									) {
										var iPrice = Game.market.calcTransactionCost(
											iAmount,
											Memory.config.oBases[ibase].sRoom, oOrders[iOrder].roomName
										);
										if (iPrice < iTolaratedCost && iDeals > 0) {
											iDeals--;
											var rtn = Game.market.deal(
												oOrders[iOrder].id,
												iAmount,
												Memory.config.oBases[ibase].sRoom
											);
											console.log(
												"Refined Mineral deal.[" + 
												oOrders[iOrder].price + 
												" (" +
												Memory.config.oBases[ibase].sRoom +
												")]-" + 
												rtn
											);
										}
									}
								}
							}
						}
					}
					
					
					
					
					iAmount = 5000;
					var oBase = Game.rooms[Memory.config.oBases[ibase].sRoom];
					if (
						oBase.terminal.store[RESOURCE_ENERGY] > 50000 && oBase.storage.store[RESOURCE_ENERGY] > 500000
					) {
						console.log(
							"Too much energy in room " + Memory.config.oBases[ibase].sRoom
						);
						var oOrders = Game.market.getAllOrders({
							type: ORDER_BUY,
							resourceType: RESOURCE_ENERGY
						});
						for (var iOrder in oOrders) {
							var iPrice = Game.market.calcTransactionCost(
								iAmount,
								Memory.config.oBases[ibase].sRoom, oOrders[iOrder].roomName
							);
							if (
								iPrice < iTolaratedCost && oBase.terminal.store[RESOURCE_ENERGY] > iPrice + iAmount &&
								iDeals > 0
							) {
								iDeals--;
								var rtn = Game.market.deal(
									oOrders[iOrder].id,
									iAmount,
									Memory.config.oBases[ibase].sRoom
								);
								console.log(
									"Energy deal in room "+ Memory.config.oBases[ibase].sRoom + ". (Purge)[" + oOrders[iOrder].price + "-" + iAmount +"]-" + rtn
								);
								break ;
							}
						}
					}
				}
			}
		}
	},
	run : function (m2) {
		let usid = m2.usages.open("market");
		m2_market.trade(m2);
		m2.usages.close(usid);
	}
};
module.exports = m2_market;
