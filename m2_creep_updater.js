
var m2_creep_updater = {
	sName: 'module_creep_updater',
	sRole : 'updater',
	bSpawnLock : false,
	arrUnitsLimits : [0,0,2,2,2,2,1,1,1],
	arrBodies: [
		[null],
		[null],
		[WORK, WORK, WORK,MOVE, MOVE, MOVE,CARRY, CARRY],
		[WORK, WORK, WORK, WORK, WORK,MOVE, MOVE, MOVE, MOVE,CARRY, CARRY],
		[WORK, WORK, WORK, WORK, WORK, WORK, MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY],
	  	[WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK, MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE, CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
	  	[WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
	  	[WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,WORK,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
	  	[
			WORK,WORK,WORK,WORK,WORK,
			MOVE,MOVE,MOVE,MOVE,MOVE,
			CARRY,CARRY,CARRY,CARRY
		]
	],
	arrUnitsByRoom : Array(),
	fnc : function (creep, preehook, m2) {
			let rTarget = [];
			let	oTarget = null;
			let oaction = null;

			if(creep.store[RESOURCE_ENERGY] == 0) {
			   creep.memory.iMode = 0;
			}
			if(creep.memory.iMode == 0 && creep.store.getFreeCapacity() == 0) {
				creep.memory.fe = 0;
				creep.memory.iMode = 2;
				if (creep.room.controller != undefined) {
					if (creep.room.controller.sign != undefined) {
						if (creep.room.controller.sign.username != creep.owner.username) {
							creep.memory.iMode = 1;
						}
					}
				}
			}
			switch (creep.memory.iMode) {
				case 0:
					m2.tools.findEnergy(creep);
					break ;
				case 1:
					let oAction = creep.signController(
						creep.room.controller, Memory.config.sRoomSign
					);
					if(oAction == ERR_NOT_IN_RANGE) {
						creep.moveTo(creep.room.controller);
					} else {
						creep.memory.iMode = 2;
					}
					break ;
				default:
					if(
		        		creep.upgradeController(
		        			creep.room.controller
		        		) == ERR_NOT_IN_RANGE
		        	) {
		                creep.moveTo(creep.room.controller);
		            }
					break ;
			}
	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_updater.sRole) {
			m2_creep_updater.arrUnitsByRoom.push(creep.memory.home);
			m2_creep_updater.fnc(creep, prehook, m2);
		}
	},
	basehook : function (m2, iBase) {
		let iCount = 0;
		let iLimit = m2_creep_updater.arrUnitsLimits[Memory.config.oBases[iBase].iLvl];
		for (let iB in m2_creep_updater.arrUnitsByRoom) {
			if (m2_creep_updater.arrUnitsByRoom[iB] == Memory.config.oBases[iBase].sRoom) {
				iCount++;
			}
		}
		if (iLimit - iCount > 0 && !Memory.config.masterlock) {
			// should build
			for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
				let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
				if (!oSpawn.spawning && !m2_creep_updater.bSpawnLock) {
					let arrBody = m2_creep_updater.arrBodies[Memory.config.oBases[iBase].iLvl];
					let iBuildRtn = oSpawn.spawnCreep(
						arrBody,
						'Luc ' +
							iBase +
							Math.random().toString(36).substr(2, 3)
						,
						{
							memory : {
								iMode: 0,
								hook: m2_creep_updater.sName,
								role: m2_creep_updater.sRole,
								home: Memory.config.oBases[iBase].sRoom
							}
						}
					);
					if (iBuildRtn == OK) {
						console.log("m2_creep_updater; Unit built in " + Memory.config.oBases[iBase].sRoom);
					} else if(iBuildRtn == ERR_NOT_ENOUGH_ENERGY) {
						// console.log("m2_creep_updater; No enough energy to build creep in room [" + Memory.config.oBases[iBase].sRoom + "]");
					}
					break ;
				}
			}
		}
	}
};

module.exports = m2_creep_updater;
