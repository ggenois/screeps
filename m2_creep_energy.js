
var m2_creep_energy = {
	sName: 'module_creep_energy',
	sRole : 'miner',
	bSpawnLock : false,
	arrUsedRessources : Array(),
	arrBodies: [
		[null],
		[null],
		[null],
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 3
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 4
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 5
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 6
		[WORK, WORK, WORK, WORK, WORK, MOVE, MOVE ,MOVE], // lvl 7
		[
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			WORK, WORK, WORK, WORK, WORK,
			MOVE, MOVE, MOVE, MOVE, MOVE,
			MOVE, MOVE, MOVE
		]
	],
	roomsWithMiners : Array(),
	fnc : function (creep, prehook, m2) {
		if (!prehook)
			return ( true );
		if (creep.memory.model == 1) {
			var sources = Game.getObjectById(creep.memory.source);
			if (sources)
			{
				if (sources.energy > 0)
				{
					let iHarvest = creep.harvest(sources);
					switch (iHarvest) {
						case ERR_NOT_IN_RANGE:
							if (!m2.arrModules['module_build'].isInGrid(m2, creep)) {
								let rFound = creep.room.lookForAt(
									LOOK_STRUCTURES, creep.pos.x, creep.pos.y
								);
								if (rFound.length == 0) {
									rFound = creep.room.lookForAt(
										LOOK_CONSTRUCTION_SITES, creep.pos.x, creep.pos.y
									);
								}
								if (rFound.length == 0) {
									creep.room.createConstructionSite(
										creep.pos.x,
										creep.pos.y,
										STRUCTURE_ROAD
									);
								}
							}
							creep.moveTo(sources, {
								ignoreCreeps: true
							});
							break ;
						case OK:
							let rFound = creep.room.lookForAt(
								LOOK_STRUCTURES, creep.pos.x, creep.pos.y
							);
							if (rFound.length == 0) {
								rFound = creep.room.lookForAt(
									LOOK_CONSTRUCTION_SITES, creep.pos.x, creep.pos.y
								);
							}
							if (rFound.length == 0) {
								// Probably no container under it. Should build one
								creep.room.createConstructionSite(
									creep.pos.x,
									creep.pos.y,
									STRUCTURE_CONTAINER
								);
							}
							break ;
						default:
							break ;
					}

				}
			}
		} else if (creep.memory.model == 2) {
			switch (creep.memory.iMode) {
				case 0:
					// init
					creep.memory.iActual = 0;
					let oBase = m2.tools.getBaseInMemoryByRoom(creep.room.name);
					if (oBase != null) {
						creep.memory.ar = Array();
						creep.memory.ar = oBase.arrEnergySources;
						creep.memory.source = creep.memory.ar[creep.memory.iActual];
						creep.memory.iMode = 1;
					} else {
						console.log("[C] Unable to find room data for " + creep.room.name + "in Memory. Probably off room. Try to get back...");
						creep.moveTo(Game.flags[creep.memory.home]);
					}
					break;
				case 1:
					let oEnergy = Game.getObjectById(creep.memory.ar[creep.memory.iActual]);
					if (oEnergy.energy > 0) {
						if (creep.harvest(oEnergy) === ERR_NOT_IN_RANGE)
							m2.tools.mv(creep, oEnergy);
					} else {
						if (creep.memory.ar.length > creep.memory.iActual + 1) {
							creep.memory.iActual++;
						} else {
							creep.memory.iActual = 0;
						}
					}
					break ;
				case 2:

					break ;
				default:
			}
		} else {
			creep.say("I'm born wrong :( no ? :'(");
		}

		//creep.say("hi...");
	},
	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_creep_energy.sRole || creep.memory.role == "miner_mk2") {
			m2_creep_energy.arrUsedRessources.push(creep.memory.source);
			m2_creep_energy.roomsWithMiners.push(creep.memory.home);
			m2_creep_energy.fnc(creep, prehook, m2);
		}
	},
	populateMemory : function (m2) {
		Memory.arrEnergySource = Array();
		for (let iBase in Memory.config.oBases) {
			for (let iSource in Memory.config.oBases[iBase].arrEnergySources) {
				Memory.arrEnergySource.push({
					iBase : iBase,
					sRoom : Memory.config.oBases[iBase].sRoom,
					sSource : Memory.config.oBases[iBase].arrEnergySources[iSource],
					bUsed : false
				});

			}
		}
	},
	run : function (m2) {
		if (Game.time % 25 === 0 || Memory.arrEnergySource == undefined)
			m2_creep_energy.populateMemory(m2);
		for (let i in Memory.arrEnergySource) {
			if (m2_creep_energy.arrUsedRessources.indexOf(Memory.arrEnergySource[i].sSource) >= 0) {
				Memory.arrEnergySource[i].bUsed = true;
			} else {
				Memory.arrEnergySource[i].bUsed = false;
			}
		}
		var arrRoomWithMiners = Array();
		for (let iMem in Memory.arrEnergySource) {
			if(Memory.config.oBases[Memory.arrEnergySource[iMem].iBase].iLvl < 8)
			{
				if (
					!Memory.arrEnergySource[iMem].bUsed &&
					!m2_creep_energy.bSpawnLock &&
					Memory.config.oBases[Memory.arrEnergySource[iMem].iBase].iLvl > 3
				) {
					let iBase = Memory.arrEnergySource[iMem].iBase;

					for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
						let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
						if (!oSpawn.spawning) {
							let arrBody = m2_creep_energy.arrBodies[Memory.config.oBases[iBase].iLvl];
							let iBuildRtn = oSpawn.spawnCreep(
								arrBody,
								'Tigrou ' +
									iBase +
									Math.random().toString(36).substr(2, 3)
								,
								{
									memory : {
										iMode: 0,
										model: 1,
										hook: m2_creep_energy.sName,
										source: Memory.arrEnergySource[iMem].sSource,
										role: m2_creep_energy.sRole,
										home: Memory.config.oBases[iBase].sRoom
									}
								}
							);
							if (iBuildRtn == OK) {
								console.log("m2_creep_energy; Unit < 8 built in " + Memory.config.oBases[iBase].sRoom);
							} else if(iBuildRtn == ERR_NOT_ENOUGH_ENERGY) {
								console.log("m2_creep_energy; No enough energy to build creep < 8");
							}

							break ;
						}
					}
				}
			}
		}
	},
	
	basehook : function (m2, iBase) {
		if (Memory.config.oBases[iBase].iLvl >= 8) {
			if (m2_creep_energy.roomsWithMiners.indexOf(Memory.config.oBases[iBase].sRoom) < 0) {
				if (!m2_creep_energy.bSpawnLock) {
					for (let iSpawn in Memory.config.oBases[iBase].rSpawn) {
						let oSpawn = Game.spawns[Memory.config.oBases[iBase].rSpawn[iSpawn]];
						if (!oSpawn.spawning) {
							let arrBody = m2_creep_energy.arrBodies[Memory.config.oBases[iBase].iLvl];
							let iBuildRtn = oSpawn.spawnCreep(
								arrBody,
								'Tigrou ' +
									iBase +
									Math.random().toString(36).substr(2, 3)
								,
								{
									memory : {
										iMode: 0,
										model: 2,
										hook: m2_creep_energy.sName,
										source: null,
										role: m2_creep_energy.sRole,
										home: Memory.config.oBases[iBase].sRoom
									}
								}
							);
							if (iBuildRtn == OK) {
								console.log("m2_creep_energy; Unit >= 8 built in " + Memory.config.oBases[iBase].sRoom);
							} else if(iBuildRtn == ERR_NOT_ENOUGH_ENERGY) {
								console.log("m2_creep_energy; No enough energy to build creep >= 8");
							}

							break ;
						}
					}

				}
			}
		}
	}
}

module.exports = m2_creep_energy;
