
var m2_federation = {
	basehook : function (m2, iBase) {
		
	},
	run : function (m2) {
		if (Memory.config.arrFederation == undefined || Memory.config.arrFederation == null) {
			Memory.config.arrFederation = Array();
		}
		
		
		if (Memory.config.arrFederation.length == 0) {
			let arrFederationTmp = Array();
			for (let iBase in Memory.config.oBases) {
				let oTerminal = Game.rooms[Memory.config.oBases[iBase].sRoom].terminal;
				if (oTerminal) {
					for (let iMineral in m2.config.arrMinerals) {
						if (oTerminal.store[m2.config.arrMinerals[iMineral]] < 2000) {
							arrFederationTmp.push({
								iBase:		iBase,
								iMineral:	m2.config.arrMinerals[iMineral],
								sRoom:		Memory.config.oBases[iBase].sRoom
							});
						}
					}
				}
			}
			if (arrFederationTmp.length > 0) {
				Memory.config.arrFederation = arrFederationTmp;
			}
		}
		
		if(Memory.config.arrFederation.length > 0) {
			for (let iBase in Memory.config.oBases) {
				if (iBase !== Memory.config.arrFederation[0].iBase) {
					let oTerminal = Game.rooms[Memory.config.oBases[iBase].sRoom].terminal;
					if (oTerminal) {
						if (oTerminal.store[Memory.config.arrFederation[0].iMineral] > 7000) {
							Game.rooms[Memory.config.oBases[iBase].sRoom].terminal.send(
								Memory.config.arrFederation[0].iMineral,
								2500,
								Memory.config.arrFederation[0].sRoom,
								"Globalisation: Room " + Memory.config.oBases[iBase].sRoom + " give " + Memory.config.arrFederation[0].iMineral + " to " + Memory.config.arrFederation[0].sRoom + " " +
								Math.random().toString(36).substr(2, 9)
							);
							Memory.config.arrFederation = m2.tools.spliceh(Memory.config.arrFederation);
							return true;
						}
					}
				}
			}
			Memory.config.arrFederation = m2.tools.spliceh(Memory.config.arrFederation);
		}
	}
};
module.exports = m2_federation;

