
var m2_lab = {
	sName : 'm2_lab',
	basehook : function (m2, iBase) {
		for (var iLab in Memory.config.oBases[iBase].lreact) {
			let oLabTo = Game.getObjectById(Memory.config.oBases[iBase].lreact[iLab].uidTo);
			if ((oLabTo.mineralAmount > 0 || !oLabTo.mineralAmount) && oLabTo.energy > 0 && oLabTo.mineralAmount != oLabTo.mineralCapacity && oLabTo.cooldown == 0)
			{
				var oLabSrcA = Game.getObjectById(Memory.config.oBases[iBase].lreact[iLab].uidSrcA);
				var oLabSrcB = Game.getObjectById(Memory.config.oBases[iBase].lreact[iLab].uidSrcB);
				if (oLabSrcA && oLabSrcB) {
					if (oLabSrcA.mineralAmount > 0 && oLabSrcA.energy > 0 && oLabSrcB.mineralAmount > 0 && oLabSrcB.energy > 0) {
						oLabTo.runReaction(oLabSrcA, oLabSrcB);
						//console.log ("Lab action ["+Memory.config.oBases[iBase].lreact[iLab].sName+"] done in room " + Memory.config.oBases[iBase].sRoom);
					}
				}
			}
		}
	}
}
module.exports = m2_lab;
