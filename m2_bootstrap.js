// m2_bootstrap.js

var m2_bootstrap = {
	sName : 'module_bootstrap',
	bLock : true,
	bSpawnLock : false,
	creepModels : [
		{
			sRole : 'boot_claimer',
			iUnitLimit: 1,
			rBody : [
				CLAIM,CLAIM,CLAIM,CLAIM,
				CLAIM,CLAIM, MOVE, MOVE,
				MOVE, MOVE, MOVE, MOVE
			]
		},
		{
			sRole : 'boot_loaders',
			iUnitLimit: 3,
			rBody : [
				WORK,WORK,WORK,WORK,WORK,
				WORK,WORK,WORK,WORK,WORK,
				MOVE,MOVE,MOVE,MOVE,MOVE,
				WORK,WORK,MOVE,MOVE,MOVE,
				CARRY,CARRY,CARRY,CARRY,CARRY,
	  			MOVE,MOVE,MOVE,MOVE,MOVE,
				CARRY,CARRY,CARRY,CARRY,CARRY,
	  			MOVE,MOVE,MOVE,MOVE,MOVE,
				CARRY,CARRY,CARRY,CARRY,CARRY,
	  			MOVE,MOVE,MOVE,MOVE,MOVE
	  		]
		}
	],

	gcl : function (m2) {
		if (Memory.config.oBases.length < Game.gcl.level) {
			return (true);
		}
	},
	/*
		# Lauch a colony... how do I do that..
	*/

	fnc_claimer : function (creep, prehook, m2) {
		// Claimer
		if (creep.room.name == creep.memory.rPath[0]) {
			// In the right room
			if(creep.room.controller) {
				switch (creep.claimController(creep.room.controller)) {
            		case ERR_NOT_IN_RANGE:
            			creep.moveTo(creep.room.controller);
            			break;
            		case OK:
						Memory.config.oBootstrap.iStep =
							Memory.config.oBootstrap.iStep + 1;
            			break ;
            		default:
            			//console.log(m2_bootstrap.sName + ": error with claimer " + creep.claimController(creep.room.controller));
            			break;
            	}
			}
		} else {
			// find where I am
			let iIndex = creep.memory.rPath.indexOf(creep.room.name);
			if (iIndex >= 0) {
				const exitDir = creep.room.findExitTo(creep.memory.rPath[iIndex - 1]);
				const exit = creep.pos.findClosestByRange(exitDir);
				creep.moveTo(exit);
			} else {
				// lost. should try to get home... hmm..
			}
		}
	},

	fnc_loaders : function (creep, prehook, m2) {
		if (creep.room.name == creep.memory.rPath[0]) {
			if (creep.pos.y < 2 || creep.pos.y > 46) {
				creep.moveTo(new RoomPosition(25, 25, creep.room.name));
			}
			if (creep.pos.x < 2 || creep.pos.y > 46) {
				creep.moveTo(new RoomPosition(25, 25, creep.room.name));
			}

			if (creep.room.controller.ticksToDowngrade < 4000) {
				m2.arrModules["module_creep_updater"].fnc(creep, prehook, m2);
			} else {
				m2.arrModules["module_creep_builder"].fnc(creep, prehook, m2);
			}


		} else {
			let iIndex = creep.memory.rPath.indexOf(creep.room.name);
			if (iIndex >= 0) {
				const exitDir = creep.room.findExitTo(creep.memory.rPath[iIndex - 1]);
				const exit = creep.pos.findClosestByRange(exitDir);
				creep.moveTo(exit);
			}
		}
	},

	creephook : function (creep, prehook, m2) {
		if (creep.memory.role == m2_bootstrap.creepModels[0].sRole) {
			m2_bootstrap.creepModels[0].iUnitLimit--;
			m2_bootstrap.fnc_claimer(creep, prehook, m2);
		} else if (creep.memory.role == m2_bootstrap.creepModels[1].sRole) {
			m2_bootstrap.creepModels[1].iUnitLimit--;
			m2_bootstrap.fnc_loaders(creep, prehook, m2);
		}
	},

	run : function (m2) {
		if (m2_bootstrap.bLock)
			return false;

		if (
			Memory.config.oBootstrap == undefined ||
			Memory.config.oBootstrap == null
		) {
			Memory.config.oBootstrap = {
				iStep: 0,
				sRoom: null,
				rPath: Array(),
				iX: null,
				iY: null
			};
		}

		switch (Memory.config.oBootstrap.iStep) {
			case 0:		// fin a new place
				if (Memory.config.oRouhBase.length > 0) {
					Memory.config.oBootstrap.iStep =
						Memory.config.oBootstrap.iStep + 1;
				} else {
					if (m2_bootstrap.gcl) {
						if (Memory.config.bRunProspector != true) {
							Memory.config.bRunProspector = true;
						}
					} else {
						console.log("Not enough GCL.");
					}
				}
				break ;
			case 1:		// Take that new place
				console.log(
					"m2_bootstrap : Adding the potential base to the active base."
				);
				Memory.config.oBootstrap.sRoom =
					Memory.config.oRouhBase[0].sRoom;
				Memory.config.oBootstrap.rPath.push(Memory.config.oRouhBase[0].sRoom);
				for (let iR in Memory.config.oRouhBase[0].rPath) {
					Memory.config.oBootstrap.rPath.push(Memory.config.oRouhBase[0].rPath[iR]);
				}
				Memory.config.oBootstrap.iX = Memory.config.oRouhBase[0].iX;
				Memory.config.oBootstrap.iY =
					Memory.config.oRouhBase[0].iY;
				Memory.config.oRouhBase =
					m2.tools.spliceh(Memory.config.oRouhBase);
				Memory.config.oBootstrap.iStep =
						Memory.config.oBootstrap.iStep + 1;
			case 2:	// Send a claimer to the new place !
				if (m2_bootstrap.creepModels[0].iUnitLimit > 0) {
					// Get the home who found out.
					let sHomeRoom = Memory.config.oBootstrap.rPath[
						Memory.config.oBootstrap.rPath.length - 1
					];
					// Get the room spawn.
					let oBase = m2.tools.getBaseInMemoryByRoom(sHomeRoom);
					if (oBase && !m2_bootstrap.bSpawnLock) {
						for (let iSpawn in oBase.rSpawn) {
							let oSpawn = Game.spawns[oBase.rSpawn[iSpawn]];
							if (!oSpawn.spawning) {
								let arrBody = m2_bootstrap.creepModels[0].rBody;
								let iBuildRtn = oSpawn.spawnCreep(
									arrBody,

									'Bootloaders_' +
										Math.random().toString(36).substr(2, 9)
									,
									{
										memory : {
											hook: m2_bootstrap.sName,
											iMode: 0,
											role: m2_bootstrap.creepModels[0].sRole,
											home: sHomeRoom,
											rPath : Memory.config.oBootstrap.rPath
										}
									}
								);
								if (iBuildRtn == OK) {
									console.log(m2_bootstrap.sName + "; Unit built in " + sHomeRoom);
								}
								break ;
							}
						}
					}
				}
				break ;
			case 3: // Add new base to the managed list of room (automated building).
				if (
					Memory.config.oBuildManaged == undefined ||
					Memory.config.oBuildManaged == null
				) {
					Memory.config.oBuildManaged = Array();
				}
				Memory.config.oBuildManaged.push({
					sRoom : Memory.config.oBootstrap.sRoom,
					iX : Memory.config.oBootstrap.iX,
					iY : Memory.config.oBootstrap.iY
				});
				Memory.config.oBootstrap.iStep = Memory.config.oBootstrap.iStep + 1;
				break ;
			case 4: // send in bootloaders until room is lvl 5
				if (m2_bootstrap.creepModels[1].iUnitLimit > 0 && Game.cpu.bucket > 5000) {
					// Get the home who found out.
					let sHomeRoom = Memory.config.oBootstrap.rPath[
						Memory.config.oBootstrap.rPath.length - 1
					];
					// Get the room spawn.
					let oBase = m2.tools.getBaseInMemoryByRoom(sHomeRoom);
					if (oBase && !m2_bootstrap.bSpawnLock) {
						for (let iSpawn in oBase.rSpawn) {
							let oSpawn = Game.spawns[oBase.rSpawn[iSpawn]];
							if (oSpawn) {
								let arrBody = m2_bootstrap.creepModels[1].rBody;
								let iBuildRtn = oSpawn.spawnCreep(
									arrBody,
									'Bootloaders_' +
										Math.random().toString(36).substr(2, 9)
									,
									{
										memory : {
											hook: m2_bootstrap.sName,
											iMode: 0,
											role: m2_bootstrap.creepModels[1].sRole,
											home: Memory.config.oBootstrap.rPath[0],
											rPath : Memory.config.oBootstrap.rPath
										}
									}
								);
								if (iBuildRtn == OK) {
									console.log(m2_bootstrap.sName + "; Unit built in " + sHomeRoom);
								}
								break ;
							}
						}
					}
				}
				if (Game.rooms[Memory.config.oBootstrap.sRoom].controller.level > 3) {
					Memory.config.oBootstrap.iStep = Memory.config.oBootstrap.iStep + 1;
				}
				break ;
			case 5:
				Memory.config.oBootstrap = null;
				break ;
			default:
				break ;
		}




	}
};

module.exports = m2_bootstrap;
